<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.email' => __('Invalid Email!'),
            'email.exists' => __('Email or Password doesn\'t match!'),
            'email.required' => __('Your Email is required!'),
            'password.required' => __('The password is required!'),
        ];
    }
   
   protected function failedValidation(Validator $validator) {
      if ($this->header('accept') == "application/json") {
         $errors = [];
         if ($validator->fails()) {
            $e = $validator->errors()->all();
            foreach ($e as $error) {
               $errors[] = $error;
            }
         }
         $json = ['success' => false,
                  'message' => $errors[0],
         ];
         $response = new JsonResponse($json, 200);
         
         throw (new ValidationException($validator, $response))->errorBag($this->errorBag)->redirectTo($this->getRedirectUrl());
      } else {
         throw (new ValidationException($validator))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
      }
      
   }
}
