<?php
namespace App\Http\Services;

use Illuminate\Support\Facades\Auth;

class CommonService
{
    public function checkValidId($id){
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return ['success'=>false];
        }
        return $id;
    }

}