<?php


namespace App\Http\Services;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminService {
   
   public $response;
   public function __construct() {
      $this->response = [
         'success' => FALSE,
         'message' => __('Something went wrong!')
      ];
   }
   
   public function updateProfile(Request $request){
      if (!empty($request->id)){
         $updated = User::where('id',$request->id)->update($request->all());
         if ($updated){
            $this->response['success'] = TRUE;
            $this->response['message'] = __('Profile updated successfully.');
         }
      }
      return $this->response;
   }
   
   public function changePassword(Request $request){
      if (!empty($request->id)){
         $user = User::find($request->id);
         $old_password = $request->old_password;
         if ($user) {
            if (Hash::check($old_password, $user->password)) {
               $user->password = bcrypt($request->password);
               $affected_row = $user->save();
               if (!empty($affected_row)) {
                  $this->response['success'] = TRUE;
                  $this->response['message'] = __('Password changed successfully.');
               }
            } else {
               $this->response['message'] = __('Incorrect old password.');
            }
         }else{
            $this->response['message'] = __('Invalid user.');
         }
      }
      return $this->response;
   }
   
   public function savePicture(Request $request){
      if (!empty($request->id)) {
         $user = User::find($request->id);
         if ($user){
            $picture = fileUpload($request->default_image,getImagePath('user_image'),$user->default_image ?? '');
            $user->default_image = $picture;
            $affected_row = $user->save();
            if ($affected_row){
               $this->response['success'] = TRUE;
               $this->response['message'] = __('Profile picture updated successfully.');
            }
         }else{
            $this->response['message'] = __('Invalid user.');
         }
      }
      return $this->response;
   }
   
}