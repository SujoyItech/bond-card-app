<?php


namespace App\Http\Services;


use App\Http\Controllers\NotificationController;
use App\Models\GroupType;
use App\Models\Notification;
use App\Models\UserContractList;
use App\Models\UserEmail;
use App\Models\UserPhone;
use App\Models\UserPhoneList;
use App\Models\UserSocialInformation;
use App\User;
use Hamcrest\SelfDescribing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\DeclareDeclare;

class UserContractService {
   public $response;
   public function __construct() {
      $this->response = [
         'success' => FALSE,
      ];
   }
   
   public function pullContractDetails(Request $request){
      $user = User::where('email',$request->email)->orWhere('phone',$request->phone)->first();
      
      if ($user){
         $this->response['success'] = TRUE;
         $this->response['data'] = UserService::getUserAllInformations($user->id);
         $this->response['message'] = __('User data successfully get.');
      }else{
         $this->response['success'] = FALSE;
         $this->response['message'] = __('User not found.');
       }
      return $this->response;
   }
   public function addContract(Request $request){
      
      
      $group_name = $request->group_name;
      $user_contract = $request->except('type','email','phone');
      
      if (!empty($request->email)){
         $user_contract['emails'] = $request->email.','.$request->emails;
      }
      if (!empty($request->email)){
         $user_contract['phones'] = $request->phone.','.$request->phones;
      }
      if ($request->type == 'qrcode'){
         $this->addContractByQrCode($user_contract,$group_name);
      }elseif ($request->type == 'manual'){
         $this->addContractByManual($user_contract,$group_name);
      }
      return $this->response;
   }
   
   private function addContractByQrCode($user_contract,$group_name=NULL){
      $user_service = new UserService();
      try {
         $user = Auth::user();
         $check_user_contact = UserContractList::where(['user_id'=>$user->id,'contract_id'=>$user_contract['contract_id']])->first();
         $user_contract = $this->prepareUserContractData($user,$check_user_contact,$user_contract);
         $user_contract['status'] = 'Approve';
         if ($check_user_contact){
            if ($check_user_contact->status == 'Pending'){
               $this->response['success'] = FALSE;
               $this->response['message'] = __('You have already sent bond request. User not approved yet.');
      
            }else{
               $this->response['success'] = FALSE;
               $this->response['data'] = $user_service->getUserProfile($user_contract['contract_id']);
               $this->response['message'] = __('This user is already in your contact list.');
            }
         }else{
            $contract_listed = UserContractList::create($user_contract);
            $contact = UserContractList::where('id',$contract_listed->id)->first();
            $this->bondedRequestedUser($contact,$contact->contract_id,$contact->user_id);
            $this->response['success'] = TRUE;
            $this->response['data'] = $user_service->getUserProfile($user_contract['contract_id']);
            $this->response['message'] = __('User successfully added to contact list.');
         }
      }catch (\Exception $exception){
//         $this->response['message'] = __('Something went wrong.');
         $this->response['message'] = $exception->getMessage();
      }
   }
   private function addContractByManual($user_contract,$group_name=NULL){
      $user_service = new UserService();
      try {
         $user = Auth::user();
         $check_user_contact = UserContractList::where(['user_id'=>$user->id,'contract_id'=>$user_contract['contract_id']])->first();
         if (isset($check_user_contact) && !empty($check_user_contact)){
            if ($check_user_contact->status == 'Pending'){
               $this->response['success'] = FALSE;
               $this->response['message'] = __('You have already sent bond request. User not approved yet.');
               
            }else{
               $this->response['success'] = TRUE;
               $this->response['message'] = __('This user is already in your contact list.');
            }
         }else{
            $user_contract_data = $this->prepareUserContractData($user,$check_user_contact,$user_contract);
            $user_contract_data['status'] = 'Pending';
            $contact_added = UserContractList::create($user_contract_data);
            if ($contact_added){
               $contact_list_id = $contact_added->id;
               $this->sendAddContractNotification($user_contract_data,$contact_list_id);
            }
            $this->response['success'] = TRUE;
            $this->response['message'] = __('Add request sent to the user.');
         }

      }catch (\Exception $exception){
         $this->response['message'] = $exception->getMessage();
      }
   }
   
   private function prepareUserContractData($user,$user_contract_list,$user_contract){

      $user_contract['user_id'] = $user->id;
      $contact_user = User::where('id',$user_contract['contract_id'])->first();

      if (!empty($user_contract['default_image']) && file_exists($user_contract['default_image'])){
         $user_contract['default_image'] = fileUpload($user_contract['default_image'],getImagePath('contact_image'),$user_contract_list->default_image ?? '');
      }else{
         if (isset($contact_user->default_image) && !empty($contact_user->default_image)){
            $user_contract['default_image'] = $contact_user->default_image;
         }
      }
      if (!empty($user_contract['cover_image']) && file_exists($user_contract['cover_image'])){
         $user_contract['cover_image'] = fileUpload($user_contract['cover_image'],getImagePath('contact_image'),$user_contract_list->cover_image ?? '');
      }else{
         if (isset($contact_user->cover_image) && !empty($contact_user->cover_image)){
            $user_contract['cover_image'] = $contact_user->cover_image;
         }
      }
      return $user_contract;
   }
   
   public function sendAddContractNotification($user_contract,$contact_list_id){
    
      $notification_data = [
         'type' => REQUESTED_NOTIFICATION,
         'title' => __('User add request notification'),
         'body' => Auth::user()->name.' '.__('wants to add you, would you accept?'),
         'user_id' => Auth::user()->id,
         'contact_id' => $user_contract['contract_id'],
         'contact_list_id' => $contact_list_id,
      ];
      NotificationController::createNotification($notification_data);
   }
   
   public function addContractResponse($contact_list_id,$status,$notification_id){
      try {
         $user = Auth::user();
         if ($status == ACCEPTED_NOTIFICATION){
            $approved = UserContractList::where('id',$contact_list_id)->update(['status'=> 'Approve']);
            if ($approved){
               $contact = UserContractList::where('id',$contact_list_id)->first();
               $this->bondedRequestedUser($contact,Auth::user()->id,$contact->user_id);
               Notification::where('id',$notification_id)->delete();
               $title = __('Bond request accepted');
               $body = $user->name.' '.__('accepted your bond request.');
               $bonded_title = __('Bonded successful.');
               $bonded_body = $user->name.' '.__('Bonded you.');
               $this->sendResponseNotification(ACCEPTED_NOTIFICATION,$title,$body,$contact->contract_id,$contact->user_id,$contact_list_id);
               $this->sendResponseNotification(BONDED_NOTIFICATION,$bonded_title,$bonded_body,$contact->contract_id,$contact->user_id,$contact_list_id);
            }
      
         }else if ($status == DECLINED_NOTIFICATION){
            Notification::where('id',$notification_id)->delete();
            $contact = UserContractList::where('id',$contact_list_id)->first();
            $title = __('Bond request rejected');
            $body = $user->name.' '.__('rejected your bond request.');
            $this->sendResponseNotification(DECLINED_NOTIFICATION,$title,$body,$contact->contract_id,$contact->user_id,$contact_list_id);
            UserContractList::where('id',$contact_list_id)->delete();
         }
         $this->response['success'] = TRUE;
         $this->response['message'] = __('Notification sent successfully.');

      }catch (\Exception $e){
         $this->response['success'] = FALSE;
         $this->response['message'] = __('Notification sent failed.');
      }
      
      return $this->response;
      
   }
   
   private function bondedRequestedUser($contact,$user_id,$contract_id){
      $bond_user = User::leftJoin('user_social_information','users.id','user_social_information.user_id')
      ->select('users.id','users.name','users.email','users.phone','users.address','users.time','users.default_image','users.cover_image',
      'user_social_information.facebook','user_social_information.facebook','user_social_information.instagram','user_social_information.whatsapp',
      'user_social_information.telegram')
      ->where('users.id',$contract_id)->first();
      $insert_data = [
         'name' => $bond_user->name ?? '',
         'user_id' => $user_id,
         'contract_id' => $bond_user->id,
         'status' => 'Approve',
         'group_name' => $contact->group_name ?? '',
         'address' => $bond_user->address ?? '',
         'facebook' => $user_social_data->facebook ?? '',
         'instagram' => $user_social_data->instagram ?? '',
         'whatsapp' => $user_social_data->whatsapp ?? '',
         'telegram' => $user_social_data->telegram ?? '',
         'time' => $bond_user->time ?? '',
         'default_image' => $bond_user->default_image ?? '',
         'cover_image' => $bond_user->cover_image ?? '',
      ];
      $user_emails = UserEmail::where('user_id',$contact->user_id)->pluck('email')->toArray();
      $user_phones = UserPhone::where('user_id',$contact->user_id)->pluck('phone')->toArray();
      $emails = $bond_user->email ?? '';
      $phones = $bond_user->phone ?? '';
      if (!empty($user_emails[0])){
         if (!empty($emails)){
            $emails = $emails.','.implode(',',$user_emails);
         }
      }
      if (!empty($user_phones[0])){
         if(!empty($phones)){
            $phones = $phones.','.implode(',',$user_phones);
         }
      }
      $insert_data['emails'] = $emails;
      $insert_data['phones'] = $phones;
      UserContractList::create($insert_data);
   }
   
   private function sendResponseNotification($type,$title,$body,$user_id,$contact_id,$contact_list_id){
      $notification_data = [
         'type' => $type,
         'title' => $title,
         'body' => $body,
         'user_id' => $user_id,
         'contact_id' => $contact_id,
         'contact_list_id' => $contact_list_id
      ];
      NotificationController::createNotification($notification_data);
   }
   
   public function getContractLists($type){
      $user_id = Auth::user()->id;
      $data = [];
      if (isset($user_id) && !empty($user_id)){
         $contract_lists = UserContractList::where(['user_id'=>$user_id,'status'=>'Approve']);
         if($type == 'recent'){
            $contract_lists= $contract_lists->where('contact_type',0)->orderBy('user_contract_lists.updated_at','desc');
            $contract_lists = $contract_lists->paginate(20);
         }elseif ($type == 'fav'){
            $contract_lists = $contract_lists->where('is_fav',1)->orderBy('contact_type','asc')->orderBy('name','asc');
            $contract_lists = $contract_lists->paginate(20);
         }else{
            $contract_lists = $contract_lists->orderBy('name','asc')->paginate(20);
         }
         
         if ($contract_lists){
            $this->response = [
               'success' => true,
               'data' => $contract_lists,
               'message' => 'Contact list get successfully'
            ];
            $this->response['image_path'] = asset(getImagePath('contact_image'));
         }else{
            $this->response = [
               'success' => FALSE,
               'data' => [],
               'message' => 'Something went wrong'
            ];
         }
      }else{
         $this->response = [
            'success' => FALSE,
            'data' => [],
            'message' => 'User not authorized'
         ];
      }
      return $this->response;
     
   }
   
   public function contractListDetails($contractListId){
      $contract_list = UserContractList::where('id',$contractListId)->first();
      if ($contract_list){
         $contract_list = $this->useContractNullCheck($contract_list);
         if (!empty($contract_list['emails'])){
            $contract_list['emails'] = explode(',',$contract_list['emails']);
         }if (!empty($contract_list['phones'])){
            $contract_list['phones'] = explode(',',$contract_list['phones']);
         }
         $this->response['success'] = TRUE;
         $this->response['data'] = $contract_list;
         $this->response['data']['contact_image_url'] = asset(getImagePath('contact_image'));
         $this->response['message'] = __('User contact list details get successfully.');
      }else{
         $this->response['message'] = __('User contact list not found.');
      }
      
      return $this->response;
   }
   
   private function useContractNullCheck($contract_list){
      if ($contract_list['group_id'] == NULL){
         $contract_list['group_id'] = '';
      }if ($contract_list['group_name'] == NULL){
         $contract_list['group_name'] = '';
      }if ($contract_list['name'] == NULL){
         $contract_list['name'] = '';
      }if ($contract_list['emails'] == NULL || $contract_list['emails'] == ''){
         $contract_list['emails'] = [];
      }if ($contract_list['phones'] == NULL || $contract_list['phones'] == ''){
         $contract_list['phones'] = [];
      }if ($contract_list['address'] == NULL){
         $contract_list['address'] = '';
      }if ($contract_list['facebook'] == NULL){
         $contract_list['facebook'] = '';
      }if ($contract_list['instagram'] == NULL){
         $contract_list['instagram'] = '';
      }if ($contract_list['whatsapp'] == NULL){
         $contract_list['whatsapp'] = '';
      }if ($contract_list['telegram'] == NULL){
         $contract_list['telegram'] = '';
      }if ($contract_list['time'] == NULL){
         $contract_list['time'] = '';
      }if ($contract_list['default_image'] == NULL || empty($contract_list['default_image'])){
         $contract_list['default_image'] = '';
      }if ($contract_list['cover_image'] == NULL || empty($contract_list['cover_image'])){
         $contract_list['cover_image'] = '';
      }
      return $contract_list;
   }
   
   public function deleteContractList($contractListId){
      try {
         $user_contact_data = UserContractList::where('id',$contractListId)->first();
         if (isset($user_contact_data))
         {
            if ($user_contact_data->contact_type == 1){
               UserContractList::where('id',$contractListId)->delete();
               $this->response['success'] = TRUE;
               $this->response['message'] = __('Contact data deleted successfully.');
               
            }elseif ($user_contact_data->contact_type == 0){
               $bond_user_contact_data = UserContractList::where(['user_id'=>$user_contact_data->contract_id,'contract_id'=>$user_contact_data->user_id])->first();
               UserContractList::where('id',$contractListId)->delete();
               if (isset($bond_user_contact_data) && !empty($bond_user_contact_data)){
                  UserContractList::where('id',$bond_user_contact_data->id)->delete();
                  $this->sendUnbondNotification($user_contact_data);
               }
               $this->response['success'] = TRUE;
               $this->response['message'] = __('Contact data deleted successfully.');
            }
         }
        
      }catch (\Exception $exception){
         $this->response['success'] = FALSE;
         $this->response['message'] = __('Something went wrong!');
      }
      
      return $this->response;
   }
   
   public function sendUnbondNotification($user_contact_data){
      $notification_data = [
         'type' => UNBONDED_NOTIFICATION,
         'title' => __('User Unbond notification'),
         'body' => Auth::user()->name.' '.__('unbonded you.'),
         'user_id' => Auth::user()->id,
         'contact_id' => $user_contact_data->contract_id,
         'contact_list_id' => $user_contact_data->id
      ];
      NotificationController::createNotification($notification_data);
   }
   
   public function userSyncPhoneContact(Request $request){
      DB::beginTransaction();
      try {
         $data = $request->data;
         $user_id = Auth::user()->id;
         if (isset($data) && !empty($data[0])){
            UserContractList::where(['user_id'=>$user_id,'contact_type'=>1])->delete();
            $insert_data = [];
            foreach ($data as $key=>$value){
               $insert_data [$key]['user_id'] = $user_id;
               $insert_data [$key]['contract_id'] = 0;
               $insert_data [$key]['status'] = 'Approve';
               $insert_data [$key]['group_name'] = $value['group_name'];
               $insert_data [$key]['name'] = $value['name'];
               $insert_data [$key]['emails'] = $value['emails'];
               $insert_data [$key]['phones'] = $value['phones'];
               $insert_data [$key]['address'] = $value['address'];
               $insert_data [$key]['facebook'] = $value['facebook'];
               $insert_data [$key]['instagram'] = $value['instagram'];
               $insert_data [$key]['whatsapp'] = $value['whatsapp'];
               $insert_data [$key]['telegram'] = $value['telegram'];
               $insert_data [$key]['default_image'] = $value['default_image'];
               $insert_data [$key]['cover_image'] = $value['cover_image'];
               $insert_data [$key]['contact_type'] = 1;
            }
            UserContractList::insert($insert_data);
            $this->response['success'] = true;
            $this->response['message'] = __('Phone number synced successfully.');
            DB::commit();
         }else{
            $this->response['success'] = FALSE;
            $this->response['message'] = __('No data found.');
         }
         
      }catch (\Exception $exception){
         DB::rollBack();
         $this->response['success'] = FALSE;
         $this->response['message'] = __('Something went wrong.');
      }
      return $this->response;
   }
   
   public function contractFav($contractId){
      $contract = UserContractList::where('id','=',$contractId)->first();
      
      if ($contract->is_fav == 0){
         $updated = UserContractList::where('id','=',$contractId)->update(['is_fav'=>1]);
         if ($updated){
            $this->response['success'] = true;
            $this->response['message'] = __('Contact added to favourite list');
         }
      }else{
         UserContractList::where('id','=',$contractId)->update(['is_fav'=>0]);
         $this->response['success'] = TRUE;
         $this->response['message'] = __('Contact remove from favourite list');
      }
      return $this->response;
   }
   
   
   public function searchContract(Request $request){
      $user = Auth::user();
      $text = $request->text;
      
      $data = UserContractList::where(['user_id'=>$user->id,'status'=>'Approve'])->where(function ($query) use ($text){
         $query->where('name','LIKE','%'.$text.'%')
               ->orWhere('address','LIKE','%'.$text.'%')
               ->orWhere('emails','LIKE','%'.$text.'%')
               ->orWhere('phones','LIKE','%'.$text.'%');
      })->get();
      
      if ($data){
         $this->response['success'] = true;
         $this->response['data'] = $data;
         $this->response['message'] = __('Search List get successfully.');
      }else{
         $this->response['message'] = __('No data found.');
      }
      return $this->response;
   }
   
   public function fiterSearchContract(Request $request){
      $user = Auth::user();
      $data = UserContractList::select('id','user_id','contract_id','is_fav','status','group_name','name',
         'emails','phones','address','facebook','instagram','whatsapp','telegram','time','default_image','cover_image')
      ->where(['user_id'=>$user->id]);
      if (!empty($request->group_name) && $request->group_name !==NULL){
         $data->where('group_name','like','%'.$request->group_name.'%');
      }
      if (!empty($request->name) && $request->name !==NULL){
         $data->where('name','LIKE','%'.$request->name.'%');
      }
      if (!empty($request->address) && $request->address !==NULL){
         $data->where('address','like','%'.$request->address.'%');
      }
      $data = $data->get();
      if ($data){
         $this->response['success'] = true;
         $this->response['data'] = $data;
         $this->response['message'] = __('Search List get successfully.');
      }else{
         $this->response['message'] = __('No data found.');
      }
      
      return $this->response;
   }
}