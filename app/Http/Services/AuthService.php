<?php


namespace App\Http\Services;



use App\Http\Repositories\UserAuthRepository;
use App\Http\Requests\RegistrationRequest;
use App\Mail\ForgetPasswordMail;
use App\Mail\SendRegistrationEmail;
use App\Models\UserVerificationCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthService {
   
   private $response;
   private $repository;
   
   public function __construct() {
      $this->repository = new UserAuthRepository();
      $this->response = [
         'success' => false,
      ];
   }
   
   
   public function login(Request $request){
   
      if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
         $user = Auth::user();
         $this->response['success'] = true;
         $this->response['user'] = $user;
      }else{
         $this->response['message'] = __('Email or password doesn\'t matched!');
      }
      
      return $this->response;
   }
   
   public function socialLogin(Request $request){
   
      if (!empty($request->email)){
         $user = User::where('email',$request->email)->first();
         Auth::loginUsingId($user->id);
         $user = Auth::user();
         $this->response['success'] = true;
         $this->response['user'] = $user;
      }else{
         $this->response['message'] = __('Email or password doesn\'t matched!');
      }
      
      return $this->response;
   }
   
   public function register(Request $request){
      try {
         $user = $this->repository->userDataSave($request);
         if ($user) {
            $data = ['success' => true, 'message' => __('Registration successful.')];
         } else {
            $data = ['success' => false, 'message' => __('Something went wrong.')];
         }
      } catch (\Exception $e) {
         $data = ['success' => false, 'message' => $e->getMessage()];
      }
      
      return $data;
   }
   
   public function socialRegister(Request $request){
      try {
         $user = $this->repository->userSocialDataSave($request);
         if ($user) {
            $data = ['success' => true, 'message' => __('Registration successful.')];
         } else {
            $data = ['success' => false, 'message' => __('Something went wrong.')];
         }
      } catch (\Exception $e) {
         $data = ['success' => false, 'message' => $e->getMessage()];
      }
      
      return $data;
   }
   
   public function userVerifyEmail($code)
   {
      $service = new CommonService();
      $code = $service->checkValidId($code);
      
      $user_verification = UserVerificationCode::where(['code' => $code])->first();
      $response = [];
      if ($user_verification) {
         DB::beginTransaction();
         try {
            UserVerificationCode::where(['id' => $user_verification->id])->update(['status' => STATUS_SUCCESS]);
            $user = $this->find($user_verification->user_id);
            if (!empty($user)) {
               if ($user->email_verified == STATUS_PENDING) {
                  $user_update = $user->update(['email_verified' => STATUS_SUCCESS, 'active_status' => STATUS_SUCCESS, 'email_verified_at'=> Carbon::now()]);
                  $response = ['status' => true, 'message' => __('Email successfully verified.')];
               } else {
                  $response = ['status' => true, 'message' => __('You already verified email!')];
               }
            } else {
               $response = ['status' => false, 'message' => __('Email Verification Failed')];
            }
            DB::commit();
         } catch (\Exception $e) {
            DB::rollBack();
            $response = ['status' => false, 'message' => $e->getMessage()];
         }
      } else {
         $response = ['status' => false, 'message' => __('Verification Code Not Found!')];
      }
      return $response;
   }
   
   
   public function find($id)
   {
      return User::find($id);
   }
   
   public function userMailVerification($user)
   {
      $mail_key = randomNumber(6);
      UserVerificationCode::create(['user_id' => $user->id, 'code' => $mail_key, 'type' => 1, 'status' => STATUS_PENDING, 'expired_at' => date('Y-m-d', strtotime('+15 days'))]);
      $userName = $user->name;
      $userEmail = $user->email;
      $subject = __('Bond Card Email Verification.');
      $userData['message'] = __('Hello! ') . $userName . __(' Please Verify Your Email.');
      $userData['key'] = $mail_key;
      $userData['email'] = $userEmail;
      sendMail($userEmail, $userData, $subject);
   }
   
   public function forgetPasswordMailProcess(Request $request,$type='web'){
      $user = User::where('email','=',$request->email)->first();
      try {
         if($user){
            $data['name'] = $user->name;
            $data['email'] = $user->email;
            $subject = __('Reset Password');
            $data['remember_token'] = $user->remember_token;
            $data['type'] = $type;
            $this->sendForgotPassWordMail($user->email,$data,$subject);
            $this->response['success'] = TRUE;
            $this->response['data']['verification_code'] = $user->remember_token;
            $this->response['message'] = __('Reset password mail sent successfully. Please check your email.');
         }else{
            $this->response['success'] = FALSE;
            $this->response['message'] = __('Your email is not correct !');
         }
      }catch (\Exception $e){
         $this->response['success'] = FALSE;
         $this->response['message'] = __('Something went wrong . Please try again!');
      }
      return $this->response;
     
   }
   
   public function sendForgotPassWordMail($email, $data, $subject = '')
   {
      Mail::to($email)->send(new ForgetPasswordMail($subject,$data));
   }
   
   public function updatePassword(Request $request){
      try {
         $user = User::where('remember_token',$request->token)->first();
         if ($user){
            $update_password['remember_token'] = Str::random(10);
            $update_password['password'] = Hash::make($request->password);
            $update_password['email_verified'] = STATUS_SUCCESS;
   
            $updated = User::where(['id' => $user->id])->update($update_password);
   
            if ($updated) {
               $this->response['success'] = TRUE;
               $this->response['message'] = __('Password changed successfully.');
            } else {
               $this->response['success'] = FALSE;
               $this->response['message'] = __('Password not changed try again.');
            }
         }else {
            $this->response['success'] = FALSE;
            $this->response['message'] = __('Code is not valid.');
         }
      }catch (\Exception $exception){
         $this->response['success'] = FALSE;
         $this->response['message'] = __('Something went wrong . Please try again!');
      }
      return $this->response;
   }
   
}