<?php


namespace App\Http\Services;


use App\Jobs\SendEmailJob;
use Illuminate\Support\Facades\Mail;

class MailService {
   public static function mailSendingProcess($emails,$body,$subject,$template){
    
      if (is_array($emails)){
         $collection_data = collect($emails);
         $chunk_data = $collection_data->chunk(500);
         foreach ($chunk_data as $insert_chunk_data){
            dispatch(new SendEmailJob($insert_chunk_data,$body,$subject,$template));
         }
      }else{
         dispatch(new SendEmailJob($emails,$body,$subject,$template));
      }
   }
   
   public static function sendMail($emails,$body,$subject,$template){
      Mail::send('admin.mail.reset_password_mail',array('body',$body),function ($messages) use ($emails,$subject){
         $messages->to($emails)->subject($subject);
      });
   }
   
}