<?php

namespace App\Http\Services;

use App\Http\Repositories\UserRepository;
use App\Jobs\SendNotification;
use App\Models\Notification;
use App\Models\UserContactEmail;
use App\Models\UserContactPhone;
use App\Models\UserContract;
use App\Models\UserContractList;
use App\Models\UserContractsContactInfo;
use App\Models\UserEmail;
use App\Models\UserInformation;
use App\Models\UserPhone;
use App\Models\UserSocialInformation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserService
{
   public $response;
   private $repository;
   public function __construct() {
      $this->response = [
         'success' => false,
         'data' => []
      ];
      $this->repository = new  UserRepository();
   }
   
   public function getUserInfo($userId=NULL){
      
      $users = User::query();
      if (isset($users) && !empty($users))
      {
         if($userId != null){
            $this->response['success'] = true;
            $this->response['data']['user'] = $users->where('id',$userId)->first();
            $this->response['message'] = __('User get successfully.');
         }else{
            $data = $users->where('role','<>','admin')->get();
            $this->response['success'] = true;
            $this->response['data']['user_lists'] = $data;
            $this->response['message'] = __('User get successfully.');
         }
      }
      
      return $this->response;
   }
   
   public function getUserContract($id = NULL){
      $user_contracts = UserContract::query();
      if ($id != null){
         $this->response['success'] = true;
         $this->response['data']['user_contracts'] = $user_contracts->where('user_id',$id)->get();
         $this->response['message'] = __('User contact get successfully.');
      }
      return $this->response;
   }
   
   public function getUserContractList($id=null){
      $user_lists = UserContractList::query();
      if (isset($user_lists) && !empty($user_lists)){
         if($id != null) {
            $data = $user_lists->leftJoin('users','user_contract_lists.contract_id','=','users.id')
                               ->select('users.id', 'users.name','users.email','users.phone','user_contract_lists.is_fav')
                               ->where('user_contract_lists.user_id', $id)->get();
      
         }else{
            $data = $user_lists->get();
         }
         $this->response['success'] = true;
         $this->response['data']['user_contract_lists'] = $data;
         $this->response['message'] = __('User contact get successfully.');
      }
      
      return $this->response;
   }
   
   public static function getUserAllInformations($user_id){
      $user_data = [];
      $user_data = User::leftJoin('user_social_information','users.id','user_social_information.user_id')
                                    ->select('users.*','user_social_information.facebook',
                                       'user_social_information.instagram','user_social_information.whatsapp','user_social_information.telegram')
                                    ->where('users.id',$user_id)->first();
      $user_data['user_emails'] = UserEmail::select('email')->where('user_id',$user_id)->get()->pluck('email')->toArray();
      $user_data['user_phones'] = UserPhone::select('phone')->where('user_id',$user_id)->get()->pluck('phone')->toArray();
      $user_data = self::userInformationNullCheck($user_data);
      return $user_data;
   }
   
   private static function userInformationNullCheck($user_data){
      if ($user_data['name'] == NULL){
         $user_data['name'] = "";
      } if ($user_data['email'] == NULL){
         $user_data['email'] = "";
      } if ($user_data['phone'] == NULL){
         $user_data['phone'] = "";
      } if ($user_data['country'] == NULL){
         $user_data['country'] = "";
      } if ($user_data['language'] == NULL){
         $user_data['language'] = "";
      } if ($user_data['time_zone'] == NULL){
         $user_data['time_zone'] = "";
      }if ($user_data['default_image'] == NULL){
         $user_data['default_image'] = "";
      }if ($user_data['cover_image'] == NULL){
         $user_data['cover_image'] = "";
      }if ($user_data['address'] == NULL){
         $user_data['address'] = "";
      }if ($user_data['time'] == NULL){
         $user_data['time'] = "";
      }if ($user_data['bio'] == NULL){
         $user_data['bio'] = "";
      }if ($user_data['facebook'] == NULL){
         $user_data['facebook'] = "";
      }if ($user_data['instagram'] == NULL){
         $user_data['instagram'] = "";
      }if ($user_data['whatsapp'] == NULL){
         $user_data['whatsapp'] = "";
      }if ($user_data['telegram'] == NULL){
         $user_data['telegram'] = "";
      }
      return $user_data;
   }
   public function getUserProfile($user_id){
      
      if(isset($user_id) && !empty($user_id)){
         $this->response['success'] = true;
         $this->response['data'] = $this->getUserAllInformations($user_id);
         $this->response['data']['image_path'] = asset(getImagePath('user_image'));
         $this->response['message'] = __('User Profile get successfully.');
      }
      return $this->response;
   
   }
   
   public function userEditProfile(Request $request){
    
      $user = Auth::user();
      if (isset($user) && !empty($user)){
         if (isset($request->phone) && !empty($request->phone)){
            
            $user_check = User::where('phone',$request->phone)->where('id','!=',$user->id)->first();
            
            if (isset($user_check) && !empty($user_check)){
               $this->response = ['success'=>false,'data'=>[],'message'=>__('User of this phone number already exist.')];
            }else
            {
               $data = $this->repository->userProfileUpdate($request,$user);
               
               $contact_users = UserContractList::where(['user_id'=>$user->id,'contact_type'=>0])->where('contract_id','<>',$user->id)->pluck('contract_id')->toArray();
            
               if (isset($contact_users) && !empty($contact_users)){
                  $notification = $this->editProfileNotification($user);
                  dispatch(new SendNotification($contact_users,$notification));
               }
            $this->response = $data;
            }
         }else{
            $this->response = ['success'=>false,'data'=>[],'message'=>__('Phone field is required.')];
         }
      }else{
         $this->response = ['success'=>false,'data'=>[],'message'=>__('User not found.')];
      }
      return $this->response;
      
   }
   
   private function editProfileNotification($user){
      $notification = [
         'title' => __('User profile update'),
         'body' => $user->name.__(' updated his profile.'),
         'type' => PROFILE_UPDATE_NOTIFICATION
      ];
      return $notification;
   }
   
   public function addContract(Request $request,$user_id){
      
      if (isset($request) && !empty($request)){
         $data = $this->repository->addContract($request,$user_id);
         if ($data){
            $this->response = ['success'=>TRUE,'data'=>[],'message'=>__('New contact created successfully.')];
         }else{
            $this->response = ['success'=>FALSE,'data'=>[],'message'=>__('Something went wrong!')];
         }
      }else{
         $this->response = ['success'=>FALSE,'data'=>[],'message'=>__('Insert data is empty!')];
      }
      
      return $this->response;
   }
   
   public function contractDetails($contractId){
      $user_contract_info = UserContract::where('id',$contractId)->first();
      if($user_contract_info){
         $this->response['success'] = TRUE;
         $this->response['data'] = $user_contract_info;
         $this->response['data']['contact_emails'] =  UserContactEmail::where('user_contact_id',$contractId)->get()->pluck('email');
         $this->response['data']['contact_phones'] =  UserContactPhone::where('user_contact_id',$contractId)->get()->pluck('phone');
         $this->response['data']['image_path'] =  asset(getImagePath('contact_image'));
         $this->response['message'] = __('User contact details get successfully');
      }else{
         $this->response['message'] = __('No data found');
      }
      return $this->response;
   }
   
   public function deleteContract($contractId){
      $delete = UserContract::where('id','=',$contractId)->delete();
      if($delete){
         $this->response['success'] = TRUE;
         $this->response['message'] = __('User contact info deleted successfully');
      }else{
         $this->response['message'] = __('User contact info delete failed.');
      }
      return $this->response;
   }
   
   public function userAllNotifications() {
      
      $notifications = Notification::where('user_id',Auth::user()->id)->orderBy('id','desc')->get();
      $notify_array = [];
      foreach ($notifications as $key=>$notify){
         $notify_array[$key]['id'] = $notify->id;
         $notify_array[$key]['user_id'] = $notify->user_id;
         $notify_array[$key]['notification_data'] = json_decode($notify->data);
         $notify_array[$key]['status'] = $notify->status;
         $notify_array[$key]['type'] = $notify->type;
      }
      if ($notifications){
         $this->response['success'] = TRUE;
         $this->response['data'] = $notify_array;
         $this->response['message'] = __('Notification get successfully');
       }else{
         $this->response['message'] = __('No data found');
      }
      return $this->response;
      
   }
   
   

}
