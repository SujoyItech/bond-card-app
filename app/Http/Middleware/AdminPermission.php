<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if($this->checkAdminPermission($user->role))
        return $next($request);
    }
    private function checkAdminPermission($role){
        if ($role == 'admin') {
            return true;
        }
    }
}
