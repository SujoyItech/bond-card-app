<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegistrationRequest;
use App\Http\Services\AuthService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
   private $data;
   
   protected $service;
   
   public function __construct() {
      
      $this->service = new AuthService();
      $this->data = [
         'success' => false,
      ];
   
   }
   
   public function login(UserLoginRequest $request){
      
      $login_response = $this->service->login($request);
      $status_code = 401;
      if ($login_response['success'] == true){
         $user = $login_response['user'];
         if ($user->role == 'user'){
            if ($user->email_verified == STATUS_SUCCESS){
               if ($user->active_status == STATUS_SUCCESS){
                  $this->data['success'] = TRUE;
                  $this->data['message'] = __('Successfully logged in');
                  $token_info = $this->accessTokenProcess($user, $request);
                  $user_info = $this->userInfoNullCheck($user);
                  $user_data = [
                     'access_token'   => $token_info['access_token'],
                     'access_type'  => "Bearer",
                     'email_verified' => $user->email_verified,
                     'user_info'      => $user_info,
//                     'user_photo'     => !empty($user->photo) ? asset(getImagePath('user') . $user->photo) : ''
                  ];
                  $status_code = 200;
                  $this->data['data'] = $user_data;
                  
               } else if ($user->active_status == STATUS_SUSPENDED) {
                  $this->data['message'] = __("Your account has been suspended. please contact support team to active again");
               } else if ($user->active_status == STATUS_DELETED) {
                  $this->data['message'] = __("Your account has been deleted. please contact support team to active again");
               } else if ($user->active_status == STATUS_PENDING) {
                  $this->data['message'] = __("Your account has been Pending for admin approval. please contact support team to active again");
               }elseif ($user->active_status == STATUS_BLOCKED) {
                  $this->data['message'] = __('You are blocked. Contact with the support team.');
               }
               else {
                  $this->data['message'] = __("Your Account has some problem. please contact support team.");
               }
               
            }else {
               $user_data = ['email_verified' => $user->email_verified];
               $this->data['data'] = $user_data;
               $this->data['message'] = __('Your email is not verified. Please verify your email to get full access.');
            }
            
         }else{
            $this->data['message'] = __("You are not authorised.");
         }
         
         return response()->json($this->data, $status_code);
         
      }else{
         return response()->json($login_response, $status_code);
      }
      
   }
   
   public function logout(Request $request) {
      $access_token_id = Auth::user()->token()->id;
      DB::table('oauth_access_tokens')->where('id', $access_token_id)->delete();
      $this->data = [
         'success' => TRUE,
         'data'    => ['driver' => $request->driver],
         'message' => __('Logged Out')
      ];
      return response()->json($this->data);
   }
   
   public function register(RegistrationRequest $request){
      DB::beginTransaction();
      try {
         $response = $this->service->register($request);
         $status_code = 401;
         if ($response['success'] == true){
            $user = User::where('email', $request->email)->first();
            $user = $this->userInfoNullCheck($user);
            $token_info = $this->accessTokenProcess($user, $request);
            $user_data = [
               'access_token' => $token_info['access_token'],
               'access_type'  => "Bearer",
               'user_info'    => $user
            ];
            $status_code = 200;
            $response['data'] = $user_data;
         }
         DB::commit();
      }catch (\Exception $e){
         $response = ['success'=>FALSE,'message'=>__('Something went wrong!')];
         DB::rollBack();
      }
      return response()->json($response,$status_code);
   }
   
   public function socialLogin(Request $request){
      $user = User::where('email',$request->email)->first();
      $status_code = 401;
      if (isset($user) && !empty($user)){
         if ($user->role == 'user'){
            $login_response = $this->service->socialLogin($request);
            if ($login_response['success'] == true){
               $status_code = 200;
               $response['success'] = TRUE;
               $response['data'] = $this->authResponseData($request,$user,TRUE);
               $response['message'] = __('Login successful.');
            }else{
               $response['success'] = FALSE;
               $response['success'] = __('Login failed.');
            }
         }else{
            $response['success'] = FALSE;
            $response['message'] = __('Permission denied!');
         }
         
      }else{
         $register_response = $this->service->socialRegister($request);
         if ($register_response['success'] == TRUE){
            $user = User::where('email',$request->email)->first();
            $status_code = 200;
            $response['success'] = TRUE;
            $response['data'] = $this->authResponseData($request,$user,TRUE);
            $response['message'] = __('Registration successful.');
         }else{
            $response['success'] = FALSE;
            $response['message'] = __('Registration failed!');
         }
      }
      return response()->json($response,$status_code);
   }
   
   private function authResponseData(Request $request, $user,$email_verified){
      $user = $this->userInfoNullCheck($user);
      $token_info = $this->accessTokenProcess($user, $request);
      $user_data = [
         'access_token' => $token_info['access_token'],
         'access_type'  => "Bearer",
         'user_info'    => $user,
         'email_verified' => $email_verified
      ];
      return $user_data;
   }
   
   private function userInfoNullCheck($user){
      if ($user['name'] == NULL){
         $user['name'] = "";
      }if ($user['email'] == NULL){
         $user['email'] = "";
      }if ($user['phone'] == NULL){
         $user['phone'] = "";
      }if ($user['country'] == NULL){
         $user['country'] = "";
      }if ($user['language'] == NULL){
         $user['language'] = "";
      }if ($user['time_zone'] == NULL){
         $user['time_zone'] = "";
      }if ($user['default_image'] == NULL){
         $user['default_image'] = "";
      }if ($user['cover_image'] == NULL){
         $user['cover_image'] = "";
      }if ($user['address'] == NULL){
         $user['address'] = "";
      }if ($user['time'] == NULL){
         $user['time'] = "";
      }if ($user['bio'] == NULL){
         $user['bio'] = "";
      }
      return $user;
   }
   
   public function accessTokenProcess($user, $request) {
      $data = [];
      $token_data = $user->createToken($request->email)->toArray();
      $data['access_token'] = $token_data['accessToken'];
      $token_attribute = $token_data['token']->toArray();
      $data['access_token_id'] = $token_attribute['id'];
      if ($request->device_token != '') {
        $update = DB::table('oauth_access_tokens')
           ->where('id', $data['access_token_id'])
           ->update(['device_token' => $request->device_token, 'driver' => $request->driver]);
      }
      return $data;
   }
   
   public function sendForgetPassMail(Request $request){
      $response = $this->service->forgetPasswordMailProcess($request,'mobile');
      return response()->json($response,200);
   }
   
   public function updatePassword(ResetPasswordRequest $request){
      if ($request->token) {
         $response = $this->service->updatePassword($request);
         return response()->json($response,200);
      } else {
         $response['success'] = FALSE;
         $response['message'] = __('Code not found.');
         return response()->json($response,200);
      }
   }
}
