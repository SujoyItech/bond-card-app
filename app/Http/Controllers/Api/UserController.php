<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Services\UserService;
use App\Models\UserContractList;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
   public $service;
   public function __construct() {
      $this->service = new UserService();
   }
   
   public function getProfile(){
      $user_id = Auth::user()->id;
      $response = $this->service->getUserProfile($user_id);
      return response()->json($response,200);
   }
   
   public function editProfile(Request $request){
      $response = $this->service->userEditProfile($request);
      return response()->json($response,200);
   }
   
   public function addContract(Request $request){
      $user_id = Auth::user()->id;
      $response = $this->service->addContract($request,$user_id);
      return response()->json($response,200);
   }
   
   public function userNotification(){
      $response = $this->service->userAllNotifications();
      return response()->json($response,200);
   }
   
 
   
}
