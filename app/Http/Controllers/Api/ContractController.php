<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Services\UserContractService;
use Illuminate\Http\Request;

class ContractController extends Controller
{
   public $service;
   public function __construct() {
      $this->service = new UserContractService();
   }
   
   public function pullContractDetails(Request $request){
      $response = $this->service->pullContractDetails($request);
      return response()->json($response,200);
     
   }
   public function addContract(Request $request){
      $response = $this->service->addContract($request);
      return response()->json($response,200);
   }
   
   public function addContractResponse($contact_list_id,$status,$notification_id){
      $response = $this->service->addContractResponse($contact_list_id,$status,$notification_id);
      return response()->json($response,200);
   }
   public function contractLists($type=null){
      $response = $this->service->getContractLists($type);
      return response()->json($response,200);
   }
   
   public function contractListDetails($contractListId){
      $response = $this->service->contractListDetails($contractListId);
      return response()->json($response,200);
   }
   
   public function deleteContractList($contractListId){
      $response = $this->service->deleteContractList($contractListId);
      return response()->json($response,200);
   }
   public function userSyncPhoneContact(Request $request){
      $response = $this->service->userSyncPhoneContact($request);
      return response()->json($response,200);
   }
   
   public function contractFav($contractId){
      $response = $this->service->contractFav($contractId);
      return response()->json($response,200);
   }
   
   public function searchContract(Request $request){
      $response = $this->service->searchContract($request);
      return response()->json($response,200);
   }
   public function filterSearchContract(Request $request){
      $response = $this->service->fiterSearchContract($request);
      return response()->json($response,200);
   }
}
