<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    private function update_or_create($slug,$value){
        return AdminSetting::updateOrCreate(['slug'=>$slug],['slug'=>$slug,'value'=>$value]);
    }
    
    public function deleteRecord(Request $request){
       $request_data = $request->all();
       try {
          $deleted = DB::table($request_data['table_name'])
                       ->whereIn($request_data['primary_key_field'], $request_data['deleted_ids'])
                       ->delete();
          $response = [
             'rows' => $request->rows,
             'success' => true
          ];
       }catch (\Exception $e){
          $response = [
             'success' => false
          ];
       }
       return response()->json($response);
    }
    
    public function appSetting(){
        return view('admin.settings.app_settings');
    }

    public function basicSettingsSave(Request $request){
        if(!empty($request->app_title)){
            $update = $this->update_or_create('app_title',$request->app_title);
        }
        if(!empty($request->copyright_text)){
            $update= $this->update_or_create('copyright_text',$request->copyright_text);
        }
        if(!empty($request->primary_email)){
            $update= $this->update_or_create('primary_email',$request->primary_email);
        }
        if(!empty($request->phone)){
            $update= $this->update_or_create('phone',$request->phone);
        }
        if(!empty($request->currency_symbol)){
            $update= $this->update_or_create('currency_symbol',$request->currency_symbol);
        }
        if(!empty($request->helpline)){
            $update= $this->update_or_create('helpline',$request->helpline);
        }
        if(!empty($request->lang)){
            $update= $this->update_or_create('lang',$request->lang);
        }
        if(!empty($request->address)){
            $update= $this->update_or_create('address',$request->address);
        }
        if(!empty($request->login_text)){
            $update= $this->update_or_create('login_text',$request->login_text);
        }

        $response = [
            'success'=>true,
            'message' => 'Admin settings save successfully.'
        ];
        return response()->json($response);
    }

    public function imageUploadSave(Request $request){
        try {
            if (isset($request->app_logo)) {
                AdminSetting::updateOrCreate(['slug' => 'app_logo'], ['value' => fileUpload($request['app_logo'], getImagePath('logo'), allSetting()['app_logo'] ?? '')]);
            }
            if (isset($request->sidebar_logo)) {
                AdminSetting::updateOrCreate(['slug' => 'sidebar_logo'], ['value' => fileUpload($request['sidebar_logo'], getImagePath('logo'), allSetting()['sidebar_logo'] ?? '')]);
            }
            if (isset($request->login_logo)) {
                AdminSetting::updateOrCreate(['slug' => 'login_logo'], ['value' => fileUpload($request['login_logo'], getImagePath('logo'), allSetting()['login_logo'] ?? '')]);
            }
            $response = [
                'success'=>true,
                'message' => 'Admin logo save successfully.'
            ];
        } catch (\Exception $e) {
            $response = [
                'success'=>false,
                'message' => 'Something went wrong!'
            ];
        }
        return response()->json($response);
    }
}
