<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\UserService;
use App\Models\UserContractList;
use Illuminate\Http\Request;

class UserController extends Controller
{
   public $service;
   public function __construct() {
      $this->service = new UserService();
   }
   
   public function userEntry($id=NULL){
       $data = [];
       if($id != NULL){
          $users = $this->service->getUserInfo($id);
          $data['users'] = $users['data']['user'];
          $user_contracts = $this->service->getUserContract($id);
          $data['user_contracts'] = $user_contracts['data']['user_contracts'];
       }
       return view('admin.user.user_entry',$data);
    }
    
    public function userList(Request $request){
       
       if ($request->ajax()) {
          $user_lists = $this->service->getUserInfo();
          $users = $user_lists['data']['user_lists'];
         
          return datatables($users)
             ->editColumn('active_status', function ($item) {
                if ($item->active_status == STATUS_SUCCESS) return '<span class="badge badge-success">'.__('Active').'</span>';
                else if ($item->active_status == STATUS_PENDING) return '<span class="badge badge-warning">'.__('Pending').'</span>';
                else if ($item->active_status == STATUS_BLOCKED) return '<span class="badge badge-error">'.__('Block').'</span>';
             })
             ->rawColumns(['active_status'])
             ->make(TRUE);
       }
       $data['table_name'] = 'users';
       $data['primary_key_field'] = 'id';
       return view('admin.user.user_list',$data);
    }
    
    public function userContractList(Request $request,$id=null){
       if ($request->ajax()){
          $user_contracts = $this->service->getUserContractList($id);
          $user_lists = $user_contracts['data']['user_contract_lists'];
          return datatables($user_lists)
             ->editColumn('is_fav', function ($item) {
                if($item->is_fav == 0){
                   return '<i class="fa fa-heart text-success"></i>';
                }else{
                   return '<i class="fa fa-heart"></i>';
                }
             
             })
          ->rawColumns(['is_fav'])
          ->make(TRUE);
       }
       $data['table_name'] = 'user_contract_lists';
       $data['primary_key_field'] = 'id';
       $data['id'] = $id;
       return view('admin.user.user_contract_list',$data);
       
    }
   public function updateContractList(){
      UserContractList::where('contract_id',7)->update(['status'=>'Approve']);
   }
    
}
