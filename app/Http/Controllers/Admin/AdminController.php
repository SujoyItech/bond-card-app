<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Services\AdminService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
   private $service;
   public function __construct() {
      $this->service = new AdminService();
   }
    public function adminDashboard() {
        $data = [];
        return view('admin.dashboard', $data);
    }
   
   public function profile(){
      $data['profiles'] = Auth::user();
      return view('admin.profile.profile',$data);
   }
   public function profileView(){
      $data['profiles'] = Auth::user();
      return view('admin.profile.profile_view',$data);
   }
   
   public function profileUpdate(Request $request){
      $response = $this->service->updateProfile($request);
      return response()->json($response);
   }
   
   public function passwordUpdate(Request $request){
      $response = $this->service->changePassword($request);
      return response()->json($response);
   }
   public function pictureUpdate(Request $request){
      $response = $this->service->savePicture($request);
      return response()->json($response);
   }
}
