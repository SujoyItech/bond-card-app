<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public static function createNotification($notification){
       if (!is_string($notification['contact_list_id'])){
          $notification['contact_list_id'] = strval($notification['contact_list_id']);
       }
       $notification_data = [
          'title' => $notification['title'],
          'body' => $notification['body'],
          'contact_list_id' => $notification['contact_list_id']
       ];
      
       $notify_data  = [
          'user_id' => $notification['contact_id'],
          'data' => json_encode($notification_data),
          'status' => 0,
          'type' => $notification['type'],
          'created_at' => Carbon::now()
       ];
      
       $user = static::getNotifyUsers($notification['contact_id']);
       
       $notification_id = Notification::create($notify_data);
       $notification['notification_id'] = $notification_id->id;
       $mobile_body_data = static::getMobileNotificationData($notification);
       if($user->device_tokens != null){
          $device_tokens = explode('###', $user->device_tokens);
          foreach ($device_tokens as $device_token){
             static::pushNotificationToAndroidApp($device_token, $mobile_body_data['data_arr'], $mobile_body_data['notification_arr']);
          }
       }
    }
    
    
    public static function getNotifyUsers($user_id = NULL){
       $user_query = DB::table('users')
                       ->select('users.id', DB::raw('GROUP_CONCAT(oauth_access_tokens.device_token SEPARATOR "###") AS device_tokens'))
                       ->leftJoin('oauth_access_tokens', 'users.id', '=', 'oauth_access_tokens.user_id');
       $user_query->where('role', 'user');
       $user_query->groupBy('users.id');
       if ($user_id != NULL){
          $users = $user_query->where('users.id',$user_id)->first();
       }else{
          $users = $user_query->get();
       }
       
       return $users;
    }
   
   public static function getMobileNotificationData($notification_data){
     
      $data['data_arr'] = [
         'title' => $notification_data['title'],
         'body' => $notification_data['body'],
         'type' => $notification_data['type'],
         'contact_list_id' => $notification_data['contact_list_id'],
         'is_background' => TRUE,
         'content_available' => TRUE
      ];
      $data['notification_arr'] = [
         'title' => $notification_data['title'],
         'body' => $notification_data['body']
      ];
      return $data;
   }
   
   public static function pushNotificationToAndroidApp($device_code, array $data_arr = [], array $notification_arr = []) {
      $fields['to'] = $device_code;
      $fields['delay_while_idle'] = TRUE;
      $fields['priority'] = "high";
      $fields['time_to_live'] = 3;
      if(!empty($notification_arr)){
         $fields['notification'] = $notification_arr;
      }
      $fields['data'] = $data_arr;
      //dd(env('FIREBASE_ACCESS_KEY'));
      $headers = array(
         'Authorization: key=' . env('FIREBASE_ACCESS_KEY'),
         'Content-Type: application/json'
      );
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
      $result = curl_exec($ch);
      curl_close($ch);
      return $result;
   }
   
   public static function sendNotificationToContactList($users,$notification){
      $data['data_arr'] = [
         'title' => $notification['title'],
         'body' => $notification['body'],
         'type' => $notification['type'],
         'is_background' => TRUE,
         'content_available' => TRUE
      ];
      $data['notification_arr'] = [
         'title' => $notification['title'],
         'body' => $notification['body']
      ];
      $notify_data = [
         'title' => $notification['title'],
         'body' => $notification['body'],
         'type' => $notification['type'],
      ];
      $insert_notification = self::prepareNotificationInsertData($users,$notify_data);
      $notification = Notification::insert($insert_notification);
      foreach ($users as $user){
         $contact_user = self::getNotifyUsers($user);
         if(isset($contact_user) && $contact_user->device_tokens != null){
            $device_tokens = explode('###', $contact_user->device_tokens);
            foreach ($device_tokens as $device_token){
               static::pushNotificationToAndroidApp($device_token, $data['data_arr'], $data['notification_arr']);
            }
         }
      }
      
   }
   
   private static function prepareNotificationInsertData($users,$notification){
       $new_array = [];
       foreach ($users as $key=>$user){
          $new_array[$key]['user_id'] = $user;
          $new_array[$key]['data'] = json_encode($notification);
          $new_array[$key]['status'] = 0;
          $new_array[$key]['type'] = $notification['type'];
          $new_array[$key]['created_at'] = Carbon::now();
       }
       return $new_array;
   }
  
}
