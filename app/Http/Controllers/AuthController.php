<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    private $service;
    public function __construct() {
       $this->service = new AuthService();
    }
   
   public function index(){
        return view('admin.dashboard');
    }
    
    public function login() {
        if (Auth::user()) {
            if (auth::user()->role == 'admin') {
                return redirect()->route('adminDashboard');
            } elseif (auth::user()->role == 'user') {
                return redirect()->route('userDashBoard');
            }
        } else {
            return view('auth.login');
        }
    }

    public function postLogin(UserLoginRequest $request) {
       
        $response = $this->service->login($request);
        
        if ($response['success'] == true ) {
            $user = $response['user'];
            if ($user->role == 'admin') {
                return redirect()->route('adminDashboard');
            }else{
                return redirect()->route('login')->with(['dismiss' => __('Permission Denied!')]);
            }
        } else {
            return redirect()->route('login')->with(['dismiss' => __('Email or Password Not matched!')]);
        }

    }
    
    public function register(){
       return view('auth.register');
    }
    
    public function postRegister(RegistrationRequest $request){
       $response = $this->service->register($request);
       if(isset($response)) {
          if ($response['success'] == TRUE) {
             return redirect()->route('login')->with('success', $response['message']);
          } else {
             return redirect()->route('login')->with('dismiss', $response['message']);
          }
       }else{
          return redirect()->route('login')->with('dismiss',__('Something went wrong!'));
       }
    
    }
    
    public function userVerifyEmail($code){
       
       $response = $this->service->userVerifyEmail($code);
       if (isset($response)) {
          if (isset($response['status']) && ($response['status'] == true)) {
             return redirect()->route('login')->with('success', $response['message']);
          } else {
             return redirect()->route('login')->with('dismiss', $response['message']);
          }
       } else {
          return redirect()->route('login')->with('dismiss', $response['message']);
       }
       
    }
    
    public function forgetPassword(){
       return view('auth.passwords.forget_pass');
    }
    
    public function sendForgetPassMail(Request $request){
       $response = $this->service->forgetPasswordMailProcess($request);
       if ($response['success'] == true){
          return redirect()->route('login')->with('success',$response['message']);
       }else{
          return redirect()->route('login')->with('dismiss',$response['message']);
       }
    }
    
    public function passwordChange($token){
       $data['token'] = $token;
       return view('auth.passwords.reset',$data);
    }
    
    public function updatePassword(ResetPasswordRequest $request){
       if ($request->token) {
          $response = $this->service->updatePassword($request);
          if (isset($response['success']) && ($response['success'] == true)) {
             return redirect()->route('login')->with('success', $response['message']);
          } else {
             return redirect()->route('login')->with('dismiss', $response['message']);
          }
       } else {
          return redirect()->back()->with(['dismiss' => __('Code not found!')]);
       }
    }

    public function logout(Request $request) {
        $request->session()->flush();
        Auth::logout();
        return redirect()->route('login');
    }
}
