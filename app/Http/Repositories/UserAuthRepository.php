<?php


namespace App\Http\Repositories;


use App\Models\UserContract;
use App\Models\UserInformation;
use App\Models\UserSocialInformation;
use App\Models\UserVerificationCode;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserAuthRepository {
   
   public function userDataSave(Request $request){
      $insert_data = [
         'name' => $request->name,
         'email' => $request->email,
         'phone' => $request->phone ?? NULL,
         'role' => 'user',
         'remember_token' => randomString(10),
         'active_status' => STATUS_SUCCESS,
         'email_verified' => STATUS_SUCCESS,
      ];
      if (isset($request->password) && !empty($request->password)){
         $insert_data['password'] = bcrypt($request->password);
      }else{
         $insert_data['password'] = bcrypt(12345678);
      }
      return User::create($insert_data);
   }
   
   public function userSocialDataSave(Request $request){
      $insert_data = [
         'name' => $request->name,
         'email' => $request->email,
         'phone' => $request->phone ?? NULL,
         'role' => 'user',
         'remember_token' => randomString(10),
         'active_status' => STATUS_SUCCESS,
         'email_verified' => STATUS_SUCCESS,
      ];
      if (isset($request->password) && !empty($request->password)){
         $insert_data['password'] = bcrypt($request->password);
      }else{
         $insert_data['password'] = bcrypt(12345678);
      }
      return User::create($insert_data);
   }
   

   
}