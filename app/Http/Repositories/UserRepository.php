<?php


namespace App\Http\Repositories;


use App\Models\UserContactEmail;
use App\Models\UserContactPhone;
use App\Models\UserContract;
use App\Models\UserContractsContactInfo;
use App\Models\UserEmail;
use App\Models\UserInformation;
use App\Models\UserPhone;
use App\Models\UserSocialInformation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserRepository {
   
   public function userProfileUpdate(Request $request,$user){
      $data = [
         'success' => FALSE,
         'data' => [],
         'message' => ''
      ];
      $insert_data = [
         'name' => $request->name,
         'email' => $request->email,
         'phone' => $request->phone,
         'address' => $request->address,
         'time' => $request->time,
         'bio' => $request->bio,
      ];
      
      $user_emails = explode(',',$request->user_emails);
      $user_phones = explode(',',$request->user_phones);
    
      $insert_users_email = $this->prepareContactData($user_emails,'user_id', $user->id, 'email');
      $insert_users_phone = $this->prepareContactData($user_phones,'user_id', $user->id, 'phone');
      $social_data = [
         'facebook' => $request->facebook,
         'instagram' => $request->instagram,
         'whatsapp' => $request->whatsapp,
         'telegram' => $request->telegram
      ];
      try {
         if (!empty($request->default_image) && file_exists($request->default_image)){
               $insert_data['default_image'] = fileUpload($request->default_image,getImagePath('user_image'),$user->default_image ?? '');
         }
         if (!empty($request->cover_image) && file_exists($request->cover_image)){
               $insert_data['cover_image'] = fileUpload($request->cover_image,getImagePath('user_image'),$user->cover_image ?? '');
         }
         User::where('id',$user->id)->update($insert_data);
         UserEmail::where('user_id',$user->id)->delete();
         UserPhone::where('user_id',$user->id)->delete();
         UserEmail::insert($insert_users_email);
         UserPhone::insert($insert_users_phone);
         UserSocialInformation::where('user_id',$user->id)->updateOrCreate(['user_id'=>$user->id],$social_data);
         $data['success'] = TRUE;
         $data['message'] = 'Profile updated successfully.';
  
      }catch (\Exception $e){
         $data['success'] = FALSE;
         $data['message'] = $e->getMessage();
      }
      
      return$data;
   }
   
   public function addContract(Request $request, $id){
      $insert_data = [
         'user_id' => $id,
         'name' => $request->name,
         'address' => $request->address,
         'facebook' => $request->facebook,
         'instagram' => $request->instagram,
         'whatsapp' => $request->whatsapp,
         'telegram' => $request->telegram,
         'time' => $request->time,
      ];
      if (!empty($request->contact_emails)){
         $contact_emails = explode(',',$request->contact_emails);
      }
      if (!empty($request->contact_emails)){
         $contact_phones = explode(',',$request->contact_phones);
      }
      
      DB::beginTransaction();
      try {
         if (!empty($request->default_image) && file_exists($request->default_image)){
            $insert_data['default_image'] = fileUpload($request->default_image,getImagePath('contact_image'));
         }
         if (!empty($request->cover_image) && file_exists($request->cover_image)){
            $insert_data['cover_image'] = fileUpload($request->cover_image,getImagePath('contact_image'));
         }
         $data = UserContract::create($insert_data);
         $insert_contact_email = $this->prepareContactData($contact_emails,'user_contact_id',$data->id,'email');
         $insert_contact_phone = $this->prepareContactData($contact_phones,'user_contact_id',$data->id,'phone');
         UserContactEmail::insert($insert_contact_email);
         UserContactPhone::insert($insert_contact_phone);
         $response = [
            'success' => true,
            'data' => [],
            'message' => __('Contract data saved successfully.')
         ];
         DB::commit();
         
      }catch (\Exception $e){
         $response = [
            'success' => true,
            'data' => [],
            'message' => $e->getMessage()
         ];
         DB::rollBack();
      }
      
      return $response;
   }
   
   private function prepareContactData( $user_emails, $key_id, $user_id ,$type ){
      $insert_data = [];
      $i=0;
      foreach ($user_emails as $key=>$value){
         if (!empty($value)){
            $insert_data[$i][$key_id] = $user_id;
            $insert_data[$i][$type] = $value;
            $i++;
         }
      }
      return $insert_data;
   }
   
}