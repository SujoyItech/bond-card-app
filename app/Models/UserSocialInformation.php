<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSocialInformation extends Model
{
    protected $fillable = [
      'user_id','facebook','instagram','whatsapp','telegram'
    ];
   
   public function users(){
      return $this->belongsTo('App\Models\User');
   }
}
