<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContractList extends Model
{
    protected $fillable = [
      'user_id','contract_id','is_fav','group_id','group_name','name','emails','phones','address','facebook','instagram','whatsapp','telegram','time','default_image','cover_image','status','contact_type'
    ];
    
}
