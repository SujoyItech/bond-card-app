<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserImage extends Model
{
    protected $fillable = [
      'name', 'is_default', 'is_selected', 'is_cover'
    ];
   
   public function user(){
      return $this->belongsTo('App\Models\User');
   }
}
