<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContractsContactInfo extends Model
{
    protected $fillable = [
       'user_contract_id', 'email', 'phone', 'status'
    ];
    
    public function userContract(){
       return $this->belongsTo('App\Models\UserContract');
    }
}
