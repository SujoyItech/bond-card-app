<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContract extends Model
{
    protected $fillable = [
       'user_id','name', 'address','facebook','instagram', 'whatsapp', 'telegram', 'time', 'default_image', 'cover_image'
    ];
    
   public function user(){
      return $this->belongsTo('App\Models\User');
   }
   
   public function userContractsContactInfos(){
      return $this->hasMany('App\Models\UserContractsContactInfo');
   }
   
   public function userContractsSocialInfos(){
      return $this->hasMany('App\Models\UserContractsSocialInfo');
   }
   public function userContractImages(){
      return $this->hasMany('App\Models\UserContractImage');
   }
}
