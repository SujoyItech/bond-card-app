<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContactPhone extends Model
{
   protected $fillable = ['user_contract_id','phone'];
}
