<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContractsSocialInfo extends Model
{
   protected $fillable = [
      'contracts_id', 'facebook','instagram','whatsapp','telegram'
   ];
   
   public function userContract(){
      return $this->belongsTo('App\Models\UserContract');
   }
}
