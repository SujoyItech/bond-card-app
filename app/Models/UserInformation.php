<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    protected $fillable = [
       'email','phone'
    ];
    
    public function user(){
       return $this->belongsTo('App\Models\User');
    }
}
