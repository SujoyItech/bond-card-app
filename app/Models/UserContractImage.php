<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContractImage extends Model
{
   protected $fillable = [
      'user_contract_id', 'image_title', 'is_default'
   ];
   public function userContract(){
      return $this->belongsTo('App\Models\UserContract');
   }
}
