<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject,$name,$reset_token,$type;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $data)
    {
        $this->subject = $subject;
        $this->name = $data['name'];
        $this->reset_token = $data['remember_token'];
        $this->type = $data['type'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       $e_subject = $this->subject;
       $name = $this->name;
       $reset_token = $this->reset_token;
       $type = $this->type;
        return $this->view('admin.mail.reset_password_mail',compact('name','reset_token','type'))->subject($e_subject);
    }
}
