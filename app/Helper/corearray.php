<?php

//User Roles
use App\Models\Action;

function userRole($input = null) {
    $output = [
        USER_ROLE_ADMIN => __('Admin'),
        USER_ROLE_USER => __('User'),
    ];
    if (is_null($input)) {
        return $output;
    } else {
        return $output[$input];
    }
}

//User Activity Array
function userActivity($input = null) {
    $output = [
        USER_ACTIVITY_LOGIN => __('Log In'),
    ];
    if (is_null($input)) {
        return $output;
    } else {
        return $output[$input];
    }
}

function packageMonths($input = null) {
    $output = [
        1 => '1' . ' ' . __('Month'),
        3 => '3' . ' ' . __('Months'),
        6 => '6' . ' ' . __('Months'),
        12 => '1' . ' ' . __('Year'),

    ];
    if (is_null($input)) {
        return $output;
    } else {
        return $output[$input];
    }
}

//Discount Type array
function discount_type($input = null) {
    $output = [
        DISCOUNT_TYPE_FIXED => __('Fixed'),
        DISCOUNT_TYPE_PERCENTAGE => __('Percentage')
    ];
    if (is_null($input)) {
        return $output;
    } else {
        return $output[$input];
    }
}

function statusAction($input = null) {
    $output = [
        STATUS_PENDING => __('Pending'),
        STATUS_SUCCESS => __('Active'),
        STATUS_FINISHED => __('Finished'),
        STATUS_SUSPENDED => __('Suspended'),
        STATUS_REJECT => __('Rejected'),
        STATUS_DELETED => __('Deleted'),
        STATUS_BLOCKED => __('Blocked'),

    ];
    if (is_null($input)) {
        return $output;
    } else {
        return $output[$input];
    }
}

function status($input = null) {
    $output = [
        STATUS_ACTIVE => __('Active'),
        STATUS_DEACTIVE => __('Deactive'),
    ];
    if (is_null($input)) {
        return $output;
    } else {
        return $output[$input];
    }
}

function actions($input = null) {
    if (is_null($input)) {
        return Action::all()->toArray();
    } elseif (is_array($input)) {
        return Action::whereIn('name', $input)->orWhereIn('url', $input)->get()->toArray();
    } else {
        return Action::where('name', $input)->orWhere('url', $input)->first()->toArray();
    }
}

function customPages($input = null) {
    $output = [
        'faqs' => __('FAQS'),
        't_and_c' => __('T&C')
    ];
    if (is_null($input)) {
        return $output;
    } else {
        return $output[$input];
    }
}






/*********************************************
 *        End Ststus Functions
 *********************************************/
