<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'name', 'email','phone', 'country', 'language', 'password', 'remember_token', 'active_status', 'notification', 'darkmode',
       'time_zone', 'role', 'email_verified', 'email_verified_at','default_image','cover_image','address','time','bio'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    public function user_emails(){
       return $this->hasMany('\App\Models\UserEmail');
    }
    
    public function user_phones(){
       return $this->hasMany('\App\Models\UserPhone');
    }
    
    public function user_social_informations(){
      return $this->hasOne('\App\Models\UserSocialInformation');
    }
   
    public function userContractLists(){
       return $this->hasMany('\App\Models\UserContractList');
    }
    
}
