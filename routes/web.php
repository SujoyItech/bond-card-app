<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/post-register', 'AuthController@postRegister')->name('postRegister');
Route::get('email-verify/{code}','AuthController@userVerifyEmail')->name('userVerifyEmail');

Route::get('/login', 'AuthController@login')->name('login');
Route::get('/', 'AuthController@login')->name('login');
Route::post('/post-login', 'AuthController@postLogin')->name('postLogin');
Route::get('/logout', 'AuthController@logout')->name('logout');

Route::get('forget-password', 'AuthController@forgetPassword')->name('forgetPassword');
Route::post('send-forget-password-mail', 'AuthController@sendForgetPassMail')->name('sendForgetPassMail');

Route::get('password-change/{reset_token}', 'AuthController@passwordChange')->name('passwordChange');
Route::post('update-password', 'AuthController@updatePassword')->name('password.update');

//Route::get('/home', 'HomeController@index')->name('home');
