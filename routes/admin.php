<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/bond-admin', 'AdminController@adminDashboard')->name('adminDashboard');

Route::get('profile', 'AdminController@profile')->name('admin.profile');
Route::get('profile-view', 'AdminController@profileView')->name('admin.profile.view');
Route::post('admin-profile-update', 'AdminController@profileUpdate')->name('admin.profile.update');
Route::post('admin-password-update', 'AdminController@passwordUpdate')->name('admin.profile.password.update');
Route::post('admin-picture-save', 'AdminController@pictureUpdate')->name('admin.profile.picture.update');

Route::get('/app-settings', 'SettingController@appSetting')->name('appSetting');
Route::post('delete-record', 'SettingController@deleteRecord')->name('deleteRecord');
Route::post('basic-settings-save', 'SettingController@basicSettingsSave')->name('basicSettingsSave');
Route::post('image-upload-save', 'SettingController@imageUploadSave')->name('imageUploadSave');

Route::get('user/{id?}', 'UserController@userEntry')->name('userEntry');
Route::get('user-list', 'UserController@userList')->name('userList');

Route::get('user-contract-list/{id?}', 'UserController@userContractList')->name('userContractList');

Route::get('update-all-list', 'UserController@updateContractList')->name('updateContractList');

