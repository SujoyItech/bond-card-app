<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login','AuthController@login');
Route::post('register','AuthController@register');
Route::post('reset-password','AuthController@sendForgetPassMail');
Route::post('update-password','AuthController@updatePassword');
Route::post('social-login', 'AuthController@socialLogin')->name('social-login');

Route::group(['middleware' => ['auth:api', 'user.api']], function (){
   
   Route::post('logout','AuthController@logout');
   Route::get('get-profile','UserController@getProfile');
   Route::post('edit-profile','UserController@editProfile');
   
   Route::post('pull-contract-details','ContractController@pullContractDetails');
   Route::post('add-contract','ContractController@addContract');
   Route::get('contract-add-response/{contact_list_id}/{status}/{notification_id}','ContractController@addContractResponse');
   
   Route::get('contract-list-details/{contractListId}','ContractController@contractListDetails');
   Route::get('delete-contract-list/{contractListId}','ContractController@deleteContractList');
   
   Route::post('user-sync-phone-contact','ContractController@userSyncPhoneContact');
   
   //Contract lists
   Route::get('contract-lists/{type?}','ContractController@contractLists');
   Route::get('fav-contract/{contractId}','ContractController@contractFav');
   
   //search
   Route::post('search','ContractController@searchContract');
   Route::post('filter-search','ContractController@filterSearchContract');
   
   Route::get('user-notification','UserController@userNotification');
   
});

