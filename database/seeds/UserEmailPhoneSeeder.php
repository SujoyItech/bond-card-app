<?php

use Illuminate\Database\Seeder;

class UserEmailPhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users = \App\User::all();
       foreach ($users as $user){
          $user_emails = $user->user_emails()->saveMany(factory(\App\Models\UserEmail::class,5)->make());
          $user_phones = $user->user_phones()->save(factory(\App\Models\UserPhone::class,5)->make());
       }
    }
}
