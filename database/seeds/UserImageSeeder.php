<?php

use Illuminate\Database\Seeder;

class UserImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();
        foreach ($users as $user){
           $user->userImages()->saveMany(factory(\App\Models\UserImage::class,5)->make());
        }
    }
}
