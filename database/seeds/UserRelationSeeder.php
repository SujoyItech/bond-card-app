<?php

use Illuminate\Database\Seeder;

class UserRelationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $users = factory(\App\User::class,100)->create();
       
       foreach ($users as $user){
          $user_social_information = $user->user_social_informations()->save(factory(\App\Models\UserSocialInformation::class)->make());
          $user_contract_list = $user->userContractLists()->saveMany(factory(\App\Models\UserContractList::class,15)->make());
       }
    }
}
