<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'name' => 'Mr Admin',
                'email' => 'admin@email.com',
                'password' => bcrypt('123456'),
                'role' => 'admin',
                'active_status' => 1,
                'email_verified' => 1,
            ]
        );
        User::create(
            [
                'name' => 'Mr Customer',
                'email' => 'customer@email.com',
                'password' => bcrypt('123456'),
                'role' => 'user',
                'active_status' => 1,
                'email_verified' => 1,
            ]
        );
        
        
    }
}
