<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ModelsUserSocialInformation;
use Faker\Generator as Faker;

$factory->define(App\Models\UserSocialInformation::class, function (Faker $faker) {
    return [
        'facebook' => $faker->url,
        'instagram' => $faker->url,
        'whatsapp' => $faker->url,
        'telegram' => $faker->url,
    ];
});
