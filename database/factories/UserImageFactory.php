<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserImage;
use Faker\Generator as Faker;

$factory->define(UserImage::class, function (Faker $faker) {
    return [
        'name' => $faker->imageUrl(),
        'is_default' => $faker->boolean(20),
        'is_selected' => $faker->boolean(20),
        'is_cover' => $faker->boolean(20)
    ];
});
