<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\UserInformation::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'phone' => $faker->e164PhoneNumber,
    ];
});
