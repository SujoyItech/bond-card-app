<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\UserPhone::class, function (Faker $faker) {
    return [
        'phone' => $faker->e164PhoneNumber,
    ];
});
