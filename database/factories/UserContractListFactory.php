<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\AppModelsUserContractList;
use Faker\Generator as Faker;

$factory->define(\App\Models\UserContractList::class, function (Faker $faker) {
    return [
        'contract_id' => $faker->randomNumber(2),
        'is_fav' => $faker->boolean(30)
    ];
});
