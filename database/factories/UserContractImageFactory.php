<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserContractImage;
use Faker\Generator as Faker;

$factory->define(UserContractImage::class, function (Faker $faker) {
    return [
        'image_title' => $faker->imageUrl(),
        'is_default'=> $faker->boolean(50),
        'is_cover'=> $faker->boolean(50),
    ];
});
