<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

$factory->define(App\Models\UserContractsSocialInfo::class, function (Faker $faker) {
    return [
       'facebook' => $faker->url,
       'instagram' => $faker->url,
       'whatsapp' => $faker->url,
       'telegram' => $faker->url,
    ];
});
