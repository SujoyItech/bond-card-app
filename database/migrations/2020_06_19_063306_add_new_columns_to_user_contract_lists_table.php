<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnsToUserContractListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_contract_lists', function (Blueprint $table) {
           $table->string('name')->nullable();
           $table->string('emails')->nullable();
           $table->string('phones')->nullable();
           $table->string('address')->nullable();
           $table->string('facebook')->nullable();
           $table->string('instagram')->nullable();
           $table->string('whatsapp')->nullable();
           $table->string('telegram')->nullable();
           $table->string('time')->nullable();
           $table->string('default_image')->nullable();
           $table->string('cover_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_contract_lists', function (Blueprint $table) {
            $table->dropColumn(['emails','phone','address', 'facebook', 'instagram', 'whatsapp', 'telegram', 'time', 'default_image', 'cover_image']);
        });
    }
}
