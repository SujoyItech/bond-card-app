<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <title>{{__('Log In | Bond Card')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('admin/')}}/assets/images/favicon.ico">

    <!-- App css -->
    <link href="{{asset('admin/')}}/assets/css/bootstrap-modern.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="{{asset('admin/')}}/assets/css/app-modern.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />
    <link href="{{asset('admin/')}}/assets/css/bootstrap-modern-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />
    <link href="{{asset('admin/')}}/assets/css/app-modern-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet"  disabled />
    <link href="{{asset('admin/')}}/assets/css/icons.min.css" rel="stylesheet" type="text/css" />

</head>

<body class="authentication-bg authentication-bg-pattern">

<div class="account-pages mt-5 mb-5">
    @yield('content')
</div>
<!-- end page -->


<!-- Vendor js -->
<script src="{{asset('admin/')}}/assets/js/vendor.min.js"></script>

<!-- App js -->
<script src="{{asset('admin/')}}/assets/js/app.min.js"></script>

</body>
</html>
