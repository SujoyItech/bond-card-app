<div class="tab-pane" id="basictab2">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">{{__('Logo')}}</h4>
                    <form  id="logo_id" method="POST" action="{{url('image-upload-save')}}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="mt-3">
                                    <label class="col-form-label" for="app_logo">{{__('App Logo')}}</label>
                                    <input type="file" name="app_logo" data-plugins="dropify"
                                           data-default-file="{{isset( $all_settings['app_logo'] ) && file_exists(getImagePath('logo').$all_settings['app_logo']) ? asset(getImagePath('logo').$all_settings['app_logo']) : ''}}"
                                           data-allowed-file-extensions="png jpg jpeg jfif"
                                           data-max-file-size="2M" />
                                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="mt-3">
                                    <label class="col-form-label" for="sidebar_logo">{{__('Sidebar Logo')}}</label>
                                    <input type="file" data-plugins="dropify" name="sidebar_logo"
                                           data-default-file="{{isset( $all_settings['sidebar_logo'] ) && file_exists(getImagePath('logo').$all_settings['sidebar_logo']) ? asset(getImagePath('logo').$all_settings['sidebar_logo']) : ''}}"
                                           data-allowed-file-extensions="png jpg jpeg jfif"
                                           data-max-file-size="2M" />
                                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="mt-3">
                                    <label class="col-form-label" for="login_logo">{{__('Login Logo')}}</label>
                                    <input type="file" data-plugins="dropify" name="login_logo"
                                           data-default-file="{{isset( $all_settings['login_logo'] ) && file_exists(getImagePath('logo').$all_settings['login_logo']) ? asset(getImagePath('logo').$all_settings['login_logo']) : ''}}"
                                           data-allowed-file-extensions="png jpg jpeg jfif"
                                           data-max-file-size="2M" />
                                    <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                                </div>
                            </div>

                        </div>
                        <div class="row mt-3">
                            <div class="col-lg-4">
                                <div class="form-group m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light submit_logo"><i class="fa fa-save"></i> {{__('Save')}}</button>
                                </div>
                            </div>

                        </div>

                    </form>

                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $('.submit_logo').on('click', function (e) {
        Ladda.bind(this);
        var load = $(this).ladda();
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e) {
            if (!e.isDefaultPrevented()) {
                e.preventDefault();
                var formData = new FormData(this);
                makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                    load.ladda('stop');
                    var redirect_url = "{{Request::url()}}"
                    //swalSuccess(response.message);
                    if (response.success == true){
                        swalRedirect(redirect_url, response.message, 'success');
                    }else{
                        swalRedirect(redirect_url, 'Something went wrong!', 'error');
                    }
                })
            }
        });
    });
</script>
