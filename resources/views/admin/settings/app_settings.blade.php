@extends('admin.layouts.master')
@section('title','Dashboard')
@section('style')
    <link href="{{asset('admin/')}}/assets/libs/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/')}}/assets/libs/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @php( $all_settings = allSetting())
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">{{__('App Settings')}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-3"></h4>
                            <div id="basicwizard">
                                <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-4">
                                    <li class="nav-item">
                                        <a href="#basictab1" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" id="basicTabHeader1">
                                            <i class="mdi mdi-account-circle mr-1"></i>
                                            <span class="d-none d-sm-inline">{{__('General')}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#basictab2" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" id="basicTabHeader2">
                                            <i class="mdi mdi-face-profile mr-1"></i>
                                            <span class="d-none d-sm-inline">{{__('Logo')}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#basictab3" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" id="basicTabHeader3">
                                            <i class="mdi mdi-checkbox-marked-circle-outline mr-1"></i>
                                            <span class="d-none d-sm-inline">{{__('Others')}}</span>
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content b-0 mb-0 pt-0">
                                    @include('admin.settings.general_settings')
                                    @include('admin.settings.logo_settings')
                                    @include('admin.settings.others_settings')
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection

@section('script')
    <script src="{{asset('admin/')}}/assets/libs/dropzone/min/dropzone.min.js"></script>
    <script src="{{asset('admin/')}}/assets/libs/dropify/js/dropify.min.js"></script>
    <script src="{{asset('admin/')}}/assets/js/pages/form-fileuploads.init.js"></script>

    <script>
        $(document).ready(function () {
            $('#basicTabHeader1').addClass('active show');
            $('#basictab1').addClass('active');
        })
    </script>
@endsection
