<div class="tab-pane" id="basictab1">
    <form id="basic_form" class="parsley-examples" method="post" enctype="multipart/form-data" action="{{url('basic-settings-save')}}">
        <div class="row">
            <div class="col-6">
                <div class="form-group row mb-3">
                    <div class="col-12">
                        <label class="col-form-label" for="app_title">{{__('App Title')}} <span class="text-danger">*</span></label>
                        <input type="text"  class="form-control" id="app_title" name="app_title" parsley-trigger="change" required value="{{$all_settings['app_title'] ?? old('app_title')}}">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <div class="col-12">
                        <label class="col-form-label" for="copyright_text">{{__('Copyright Text')}} <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="app_title" name="copyright_text" parsley-trigger="change" required value="{{$all_settings['copyright_text'] ?? old('copyright_text')}}">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <div class="col-12">
                        <label class="col-form-label" for="address">{{__('Address')}}</label>
                        <input type="text" class="form-control" id="address" name="address" value="{{$all_settings['address'] ?? old('address')}}">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <div class="col-12">
                        <label class="col-form-label" for="primary_email">{{__('Primary Email')}}</label>
                        <input type="email" parsley-type="email" class="form-control" id="primary_email" name="primary_email" value="{{$all_settings['primary_email'] ?? old('primary_email')}}">
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="form-group row mb-3">
                    <div class="col-12">
                        <label class="col-form-label" for="phone">{{__('Phone')}}</label>
                        <input data-parsley-type="number" type="text" class="form-control" id="phone" name="phone" value="{{$all_settings['phone'] ?? old('phone')}}">
                    </div>

                </div>
                <div class="form-group row mb-3">
                    <div class="col-12">
                        <label class="col-form-label" for="login_text">{{__('Login Text')}}</label>
                        <input type="text" class="form-control" id="login_text" name="login_text" value="{{$all_settings['login_text'] ?? old('login_text')}}">
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <div class="col-12">
                        <label class="col-form-label" for="lang">{{__('Language')}}</label>
                        <select name="lang" class="form-control mb-1">
                            @foreach(langName() as $key=>$val)
                                <option @if(isset($all_settings['lang']) && $all_settings['lang']==$key) selected @endif value="{{$val}}">{{$val}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row mb-3">
                    <div class="col-12">
                        <label class="col-form-label" for="helpline">{{__('Helpline')}}</label>
                        <input type="text" class="form-control" id="helpline" name="helpline" value="{{$all_settings['helpline'] ?? old('helpline')}}">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group m-b-0">
                    <button class="btn btn-primary waves-effect waves-light submit_general"><i class="fa fa-save"></i> {{__('Save')}}</button>
                </div>
            </div>

        </div>
    </form>
</div>
<script>
    $('.submit_general').on('click', function (e) {
        var form_id = $(this).closest('form').attr('id');
        var this_form = $('#'+form_id);
        var submit_url = $(this_form).attr('action');
        $(this_form).on('submit', function (e1) {
            if (!e1.isDefaultPrevented()) {
                e1.preventDefault();
                Ladda.bind(this);
                var load = $(this).ladda();
                var formData = new FormData(this);
                makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                    var redirect_url = "{{Request::url()}}"
                    load.ladda('stop');
                    //swalSuccess(response.message);
                    if (response.success == true){
                        swalRedirect(redirect_url, response.message, 'success');
                    }else{
                        swalRedirect(redirect_url, 'Something went wrong!', 'error');
                    }
                })
            }
        });
    });
</script>

