<div id="accordion" class="mb-3">
    <div class="card mb-1">
        <div class="card-header" id="headingOne">
            <h5 class="m-0">
                <a class="text-dark" data-toggle="collapse" href="#collapseOne" aria-expanded="false">
                    <i class="fa fa-building"></i>
                    Company
                </a>
            </h5>
        </div>

        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon
                tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil
                anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan
                excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                you probably haven't heard of them accusamus labore sustainable VHS.
            </div>
        </div>
    </div>
</div>