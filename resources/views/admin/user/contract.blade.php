<div id="accordion" class="mb-3">
    <div class="card mb-1">
        <div class="card-header" id="headingTwo">
            <h5 class="m-0">
                <a class="text-dark" data-toggle="collapse" href="#collapseTwo" aria-expanded="false">
                    <i class="fa fa-file-contract"></i>
                    Contracts
                </a>
            </h5>
        </div>

        <div id="collapseTwo" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="header-title">{{__('Contracts')}}</h4>

                        <table data-toggle="table"
                               data-search="true"
                               data-show-refresh="true"
                               data-sort-name="id"
                               data-page-list="[5, 10, 20]"
                               data-page-size="5"
                               data-pagination="true" data-show-pagination-switch="true" class="table-borderless">
                            <thead class="thead-light">
                            <tr>

                                <th data-field="first_name" data-sortable="true">{{__('First Name')}}</th>
                                <th data-field="last_name" data-sortable="true">{{__('Last Name')}}</th>
                                <th data-field="address" data-sortable="true">{{__('Address')}}</th>
                                <th data-field="created_at" data-sortable="true" data-formatter="dateFormatter">{{__('Created At')}}</th>
{{--                                <th data-field="status" data-align="center" data-sortable="true" data-formatter="statusFormatter">Status</th>--}}

                            </tr>
                            </thead>


                            <tbody>
                            @if(isset($user_contracts) &&!empty($user_contracts))
                                @foreach($user_contracts as $user_contract)
                                <tr>
                                    <td>{{$user_contract->first_name}}</td>
                                    <td>{{$user_contract->last_name}}</td>
                                    <td>{{$user_contract->address}}</td>
                                    <td>{{$user_contract->created_at}}</td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div> <!-- end col-->
                </div>

            </div>
        </div>
    </div>
</div>