@extends('admin.layouts.master')
@section('title','User List')
@section('style')

@endsection

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Users Add/Edit</h4>
                    </div>
                </div>
            </div>
            <div class="card-box">
                <form action="#" class="parsley-examples">
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="first_name">{{__('Name')}}<span class="text-danger">*</span></label>
                            <input type="text" name="name" parsley-trigger="change" required value="{{$users->name??''}}"
                                   placeholder="Enter name" class="form-control" id="name">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="email">{{__('Email address')}}<span class="text-danger">*</span></label>
                            <input type="email" name="email" parsley-trigger="change" required value="{{$users->email ?? ''}}"
                                   placeholder="Enter email" class="form-control" id="email">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="phone">{{__('Phone')}}<span class="text-danger">*</span></label>
                            <input type="text" name="phone" parsley-trigger="change" value="{{$users->phone ?? ''}}"
                                   placeholder="Enter phone" class="form-control" id="phone">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="country">{{__('Country')}}<span class="text-danger">*</span></label>
                            <select id="heard" class="form-control">
                                <option value="">{{__('Choose..')}}</option>
                                @foreach(country() as $key=>$value)
                                    <option value="{{$value}}" {{isSelect($value,$users->country ?? '')}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="country">{{__('Language')}}<span class="text-danger">*</span></label>
                            <select id="heard" class="form-control">
                                <option value="">{{__('Choose..')}}</option>
                                @foreach(langName() as $key=>$value)
                                    <option value="{{$key}}" {{isSelect($key,$users->language ?? '')}}>{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if(isset($users) && !empty($users))
                        @else
                        <div class="form-group col-md-4">
                            <label for="pass1">Password<span class="text-danger">*</span></label>
                            <input id="pass1" type="password" placeholder="Password" required
                                   class="form-control">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="passWord2">Confirm Password <span class="text-danger">*</span></label>
                            <input data-parsley-equalto="#pass1" type="password" required
                                   placeholder="Password" class="form-control" id="passWord2">
                        </div>
                        @endif

                        <div class="form-group col-md-6">
                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                <i class="fa fa-save"></i>
                                {{__('Save')}}
                            </button>
                        </div>

                    </div>
                </form>

                <div class="row">
                    <div class="col-12">
                        @include('admin.user.contract')
                       @include('admin.user.company')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
