@extends('admin.layouts.master')
@section('title','User List')
@section('style')

@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
{{--                        <a class="btn btn-primary btn-xs text-white" id="" href="{{url('userEntry')}}"><i class="fa fa-plus" aria-hidden="true"></i> {{__('New User')}}</a>--}}
                        <button class="btn btn-info btn-xs no-display" id="contract_list"><i class="fa fa-list" aria-hidden="true"></i> {{__('Contract List')}}</button>
                        <button class="btn btn-info btn-xs no-display" id="view"><i class="fa fa-eye" aria-hidden="true"></i> {{__('View')}}</button>
                        <button class="btn btn-info btn-xs no-display" id="edit"><i class="fa fa-edit" aria-hidden="true"></i> {{__('Edit')}}</button>
                        <button class="btn btn-danger btn-xs no-display" id="delete-rows"><i class="fa fa-trash" aria-hidden="true"></i> {{__('Delete')}}</button>
                    </div>
                    <h4 class="page-title">{{__('User List')}}</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="table">
                            <thead>
                            <tr>
                                <th>{{__('Id')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Email')}}</th>
                                <th>{{__('Phone')}}</th>
                                <th>{{__('Active Status')}}</th>
                            </tr>
                            </thead>

                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                processing: true,
                serverSide: true,
                pageLength: 10,
                responsive: false,
                ajax: '{{url('user-list')}}',
                order: [],
                autoWidth:false,
                createdRow: function(row,data){
                    $(row).attr('id',data.id);
                },
                columns: [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "email"},
                    {"data": "phone"},
                    {"data": "active_status",className: 'text-center'},
                ],
            });
        });

        var ids = [];
        $(document).on('click', '#example tbody tr', function(){
            if($(this).toggleClass('selected')) {
                var id = $(this).attr('id');
                if ($(this).hasClass('selected')) {
                    ids.push(id);
                } else {
                    ids.splice(ids.indexOf(id), 1);
                }
                if (ids.length == 1){
                    console.log('working');
                }
                if (ids.length == 1) {
                    $('#edit').show();
                    $('#view').show();
                    $('#contract_list').show();
                    $('#delete-rows').show();
                }else if(ids.length > 1){
                    $('#edit').hide();
                    $('#view').hide();
                    $('#contract_list').hide();
                    $('#delete-rows').show();
                }else {
                    $('#edit').hide();
                    $('#view').hide();
                    $('#contract_list').hide();
                    $('#delete-rows').hide();
                }
            }
        });

        $("#edit").on('click', function () {
            var data_id = ids[0];
            if (data_id.length === 0) {
                swalError("Please Select an Item");
                return false;
            } else {
                window.location = '<?php echo URL::to('user');?>/'+data_id;
            }
        });

        $("#contract_list").on('click', function () {
            var data_id = ids[0];
            if (data_id.length === 0) {
                swalError("Please Select an Item");
                return false;
            } else {
                window.location = '<?php echo URL::to('user-contract-list');?>/'+data_id;
            }
        });

    </script>

@endsection
