<div class="tab-pane" id="profiletab">
    <form action="{{url('admin-profile-update')}}" class="parsley-examples" method="POST" id="profile_basic">
    <div class="row">
        <div class="col-12">
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label" for="name">{{__('Name')}} <span class="text-danger">*</span></label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="name" name="name" value="{{$profiles->name}}" parsley-trigger="change" required >
                </div>
            </div>
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label" for="email">{{__('Email')}} <span class="text-danger">*</span></label>
                <div class="col-md-9">
                    <input type="email" class="form-control" id="name" name="email" value="{{$profiles->email}}" parsley-trigger="change" required>
                </div>
            </div>
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label" for="email">{{__('Phone')}} </label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="phone" name="phone" value="{{$profiles->phone}}">
                </div>
            </div>
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label" for="country">{{__('Country')}} </label>
                <div class="col-md-9">
                    <select name="country" class="form-control">
                        <option value="">{{__('Select Country')}}</option>
                        @foreach(country() as $country)
                            <option value="{{$country}}" {{isSelect($country,$profiles->country)}}>{{$country}}</option>
                        @endforeach
                    </select>

                </div>
            </div>
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label" for="language">{{__('Language')}} </label>
                <div class="col-md-9">
                    <select name="language" class="form-control">
                        <option value="">{{__('Select Language')}}</option>
                        @foreach(langName() as $lang)
                        <option value="{{$lang}}" {{isSelect($lang,$profiles->language)}}>{{$lang}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row mb-3">
                <label class="col-md-3 col-form-label" for="country">{{__('Time Zone')}} </label>
                <div class="col-md-9">
                    <select name="time_zone" class="form-control">
                        <option value="">{{__('Select Time Zone')}}</option>
                        @foreach(timezone() as $key=>$value)
                            <option value="{{$key}}" {{isSelect($key,$profiles->time_zone)}}>{{$value}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row mb-3">
                <input type="hidden" name="id" value="{{$profiles->id}}">
                <button class="btn btn-primary submit_info" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            </div>
        </div>
    </div>
    </form>
</div>