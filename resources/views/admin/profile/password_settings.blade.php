<div class="tab-pane fade" id="passwordtab">
    <form action="{{url('admin-password-update')}}" class="parsley-examples" method="POST" id="profile_password">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label for="pass1">{{__('Old Password')}}<span class="text-danger">*</span></label>
                <input type="password" name="old_password" placeholder="Password" required
                       class="form-control">
            </div>
            <div class="form-group">
                <label for="pass1">{{__('New Password')}}<span class="text-danger">*</span></label>
                <input id="pass1" type="password" minlength="8" name="password" placeholder="Password" required
                       class="form-control">
            </div>
            <div class="form-group">
                <label for="passWord2">{{__('Confirm Password')}} <span class="text-danger">*</span></label>
                <input data-parsley-equalto="#pass1" minlength="8" type="password" required name="confirm_password"
                       placeholder="Password" class="form-control" id="passWord2">
            </div>

            <div class="form-group row mb-3">
                <input type="hidden" name="id" value="{{$profiles->id}}">
                <button class="btn btn-primary submit_info" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
            </div>
        </div>
    </div>
    </form>
</div>