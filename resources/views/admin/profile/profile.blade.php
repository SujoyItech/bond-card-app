@extends('admin.layouts.master')
@section('title','Profile')
@section('style')

@endsection

@section('content')
    @php( $all_settings = allSetting())
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">{{__('Profile Settings')}}</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title mb-3"></h4>
                            <div id="basicwizard">
                                <ul class="nav nav-pills bg-light nav-justified form-wizard-header mb-4">
                                    <li class="nav-item">
                                        <a href="#profiletab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" id="profile">
                                            <i class="fa fa-user-circle"></i>
                                            <span class="d-none d-sm-inline">{{__('Profile')}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#passwordtab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" id="password">
                                            <i class="fa fa-key"></i>
                                            <span class="d-none d-sm-inline">{{__('Password')}}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#picturetab" data-toggle="tab" class="nav-link rounded-0 pt-2 pb-2" id="picture">
                                            <i class="fa fa-file-image"></i>
                                            <span class="d-none d-sm-inline">{{__('Picture')}}</span>
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content b-0 mb-0 pt-0">
                                    @include('admin.profile.profile_settings')
                                    @include('admin.profile.password_settings')
                                    @include('admin.profile.picture_settings')
                                </div>
                            </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#profile').addClass('active show');
            $('#profiletab').addClass('active');
        });
        $('.submit_info').on('click', function (e) {
            Ladda.bind(this);
            var load = $(this).ladda();
            var form_id = $(this).closest('form').attr('id');
            var this_form = $('#'+form_id);
            var submit_url = $(this_form).attr('action');
            $(this_form).on('submit', function (e) {
                if (!e.isDefaultPrevented()) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    makeAjaxPostFile(formData, submit_url, load).done(function (response) {
                        load.ladda('stop');
                        //swalSuccess(response.message);
                        if (response.success == true){
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'success');
                        }else{
                            var redirect_url = "{{Request::url()}}";
                            swalRedirect(redirect_url, response.message, 'error');
                        }
                    })
                }
            });
        });
    </script>
@endsection
