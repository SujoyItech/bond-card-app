<div class="tab-pane" id="picturetab">
    <div class="row">
        <div class="col-12">
            <div class="text-center">
                <form action="{{url('admin-picture-save')}}" class="parsley-examples" method="POST" id="submit_admin_picture" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-4 col-xl-3">
                            <div class="form-group">
                                <label class="col-form-label" for="picture">{{__('Profile Picture')}}</label>
                                <input type="file" name="default_image" data-plugins="dropify"
                                       data-default-file="{{isset( $profiles->default_image) && file_exists(getImagePath('user_image').$profiles->default_image) ? asset(getImagePath('user_image').$profiles->default_image) : ''}}"
                                       data-allowed-file-extensions="png jpg jpeg jfif"
                                       data-max-file-size="2M" />
                                <p class="text-muted text-center mt-2 mb-0">{{__('Please upload jpg or png file and size should be under 2mb')}}</p>
                            </div>
                            <div class="form-group row mb-3">
                                <input type="hidden" name="id" value="{{$profiles->id}}">
                                <button class="btn btn-primary submit_info text-center" type="submit"><i class="fa fa-save"></i> {{__('Save')}}</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>