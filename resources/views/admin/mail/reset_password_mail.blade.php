@extends('admin.mail.master')
@section('content')
<table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope
       itemtype="http://schema.org/ConfirmAction"
       style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; margin: 0; border: none;"
>
    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
        <td class="content-wrap"
            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;padding: 30px;border: 3px solid #4fc6e1;border-radius: 7px; background-color: #fff;"
            valign="top">
            <meta itemprop="name" content="Confirm Email"
                  style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"/>
            <table width="100%" cellpadding="0" cellspacing="0"
                   style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block"
                        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                        valign="top">
                        <h3>{{__('Hello ')}} {{$name ?? ''}}</h3>
                        @if($type == 'mobile')
                            {{__('Reset your password by coping the key below.')}}
                        @else
                            {{__('Reset your password by clicking the link below.')}}
                        @endif

                    </td>
                </tr>

                <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                    <td class="content-block" itemprop="handler" itemscope
                        itemtype="http://schema.org/HttpActionHandler"
                        style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                        valign="top">
                        @if($type == 'web')
                        <a href="{{url('password-change/'.$reset_token)}}" class="btn-primary" itemprop="url"
                           style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #6658dd; margin: 0; border-color: #6658dd; border-style: solid; border-width: 8px 16px;">{{__('Send Reset Password Mail')}}</a>
                        @elseif($type == 'mobile')
                            <p>{{$reset_token}}</p>
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
@endsection