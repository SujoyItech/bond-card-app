<!DOCTYPE html>
<html lang="en">
@php($all_settings = allSetting())
<head>

    <meta charset="utf-8" />
    <title> @yield('title') | Bond Card</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="shortcut icon" type="image/png" href="{{isset( $all_settings['favicon'] ) && file_exists(getImagePath('logo').$all_settings['favicon']) ? asset(getImagePath('logo').$all_settings['favicon']) : ''}})}}" alt="BC"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    @include('admin.layouts.header')

</head>

<body data-layout-mode="detached" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": true}, "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": true}'>

<!-- Begin page -->
<div id="wrapper">

    @include('admin.layouts.topbar')

    @include('admin.layouts.leftmenu')

    <div class="content-page">
        @yield('content')

        @include('admin.layouts.footer')

    </div>

</div>

@include('admin.layouts.script')

</body>
</html>
