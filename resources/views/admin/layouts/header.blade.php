<link rel="shortcut icon" href="{{asset('admin/')}}/assets/images/favicon.ico">

<link href="{{asset('admin/')}}/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/')}}/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/')}}/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/')}}/assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<link href="{{asset('admin/')}}/assets/libs/bootstrap-table/bootstrap-table.min.css" rel="stylesheet" type="text/css" />

<link href="{{asset('admin/')}}/assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/')}}/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />

<link href="{{asset('admin/')}}/assets/css/bootstrap-modern.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
<link href="{{asset('admin/')}}/assets/css/app-modern.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

<link href="{{asset('admin/')}}/assets/css/bootstrap-modern-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />
<link href="{{asset('admin/')}}/assets/css/app-modern-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet"  disabled />

<link href="{{asset('admin/')}}/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/')}}/assets/css/admin.css" rel="stylesheet" type="text/css" />

<link href="{{asset('admin/')}}/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/')}}/assets/libs/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />

{{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--}}
<script src="{{asset('admin/')}}/assets/libs/jquery/jquery.min.js"></script>


<script src="{{asset('admin/')}}/assets/libs/parsleyjs/parsley.min.js"></script>


<script src="{{asset('admin/')}}/assets/libs/popper.js/popper.min.js"></script>

<!-- Plugin js-->

<script src="{{asset('admin/')}}/assets/js/form_submit.js"></script>
<script src="{{asset('admin/')}}/assets/js/admin.js"></script>

<link href="{{asset('admin/')}}/assets/libs/dropzone/min/dropzone.min.css" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/')}}/assets/libs/dropify/css/dropify.min.css" rel="stylesheet" type="text/css" />

@yield('style')
