<script src="{{asset('admin/')}}/assets/js/vendor.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/apexcharts/apexcharts.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/selectize/js/standalone/selectize.min.js"></script>

<script src="{{asset('admin/')}}/assets/js/pages/dashboard-1.init.js"></script>

<script src="{{asset('admin/')}}/assets/libs/sweetalert2/sweetalert2.all.min.js"></script>

<script src="{{asset('admin/')}}/assets/libs/ladda/spin.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/ladda/ladda.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/ladda/ladda.jquery.min.js"></script>

<script src="{{asset('admin/')}}/assets/libs/parsleyjs/parsley.min.js"></script>
<script src="{{asset('admin/')}}/assets/js/pages/form-validation.init.js"></script>
{{--<script src="{{asset('admin/')}}/assets/js/pages/form-validation.init.js"></script>--}}

<!-- Bootstrap Tables js -->
<script src="{{asset('admin/')}}/assets/libs/bootstrap-table/bootstrap-table.min.js"></script>
<script src="{{asset('admin/')}}/assets/js/pages/bootstrap-tables.init.js"></script>

<script src="{{asset('admin/')}}/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/datatables.net-select/js/dataTables.select.min.js"></script>

<script src="{{asset('admin/')}}/assets/js/pages/datatables.init.js"></script>

<script src="{{asset('admin/')}}/assets/js/app.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/dropzone/min/dropzone.min.js"></script>
<script src="{{asset('admin/')}}/assets/libs/dropify/js/dropify.min.js"></script>
<script src="{{asset('admin/')}}/assets/js/pages/form-fileuploads.init.js"></script>

@yield('script')

<script>
    $('#delete-rows').on('click',function () {
        Ladda.bind(this);
        var load = $(this).ladda();
        var deleted_rows = ids.length;
        if(deleted_rows === 0) {
            swalWarning("Please Select the item(s) Row from the List");
            load.ladda('stop');
        } else {
            var text = '';
            if (deleted_rows ==1){
                text = "this row ?";
            }else{
                text = deleted_rows+ " row (s)?";
            }
            swalConfirm("Do you really want to delete " + text).then(function (s) {
                if(s.value){
                    var id_list = ids;
                    var url = "{{url('delete-record')}}";
                    var data = {
                        'table_name' : "{{$table_name ?? ''}}",
                        'primary_key_field' : "{{$primary_key_field ?? ''}}",
                        'deleted_ids' : id_list,
                        'rows' : deleted_rows,
                        "_token" : "{{csrf_token()}}"
                    };
                    makeAjaxPost(data, url, load).done(function (response) {
                        if(response.success == false) {
                            swalError("Sorry, Data not deleted");
                        } else {
                            var confirm_text = '';
                            if(response.rows == 1){
                                confirm_text = response.rows+" row has been deleted";
                            }else {
                                confirm_text = response.rows + " row(s) has been deleted";
                            }
                            swalRedirect('',confirm_text);
                        }
                    });
                }else{
                    load.ladda('stop');
                }
            })
        }
    });
</script>
