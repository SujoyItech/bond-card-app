<div class="left-side-menu">
    <div class="h-100" data-simplebar>
        <div class="user-box text-center">
            @if(isset(Auth::user()->default_image) && !empty(Auth::user()->default_image))
                <img src="{{asset(getImagePath('user_image')).'/'.\Illuminate\Support\Facades\Auth::user()->default_image ?? ''}}" title="Mat Helme"
                     class="rounded-circle avatar-md">
            @endif
            <div class="dropdown">
                <a href="javascript: void(0);" class="text-dark font-weight-normal dropdown-toggle h5 mt-2 mb-1 d-block"
                   data-toggle="dropdown">{{Auth::user()->name ?? ''}}</a>
                <div class="dropdown-menu user-pro-dropdown">

                    <!-- item-->
                    <a href="{{route('admin.profile.view')}}" class="dropdown-item notify-item">
                        <i class="fe-user mr-1"></i>
                        <span>{{__('My Profile')}}</span>
                    </a>

                    <!-- item-->
                    <a href="{{route('admin.profile')}}" class="dropdown-item notify-item">
                        <i class="fe-settings mr-1"></i>
                        <span>{{__('Settings')}}</span>
                    </a>

                    <!-- item-->
                    <a href="{{route('logout')}}" class="dropdown-item notify-item">
                        <i class="fe-log-out mr-1"></i>
                        <span>{{__('Logout')}}</span>
                    </a>

                </div>
            </div>
            <p class="text-muted">Admin Head</p>
        </div>

        <div id="sidebar-menu">

            <ul id="side-menu">

                <li class="menu-title">Navigation</li>

                <li>
                    <a href="{{route('adminDashboard')}}">
                        <i data-feather="airplay"></i>
                        <span> {{__('Dashboard')}} </span>
                    </a>
                </li>

                <li class="menu-title mt-2">Apps</li>


                <li>
                    <a href="#sidebarSettings" data-toggle="collapse">
                        <i class="fa fa-cogs"></i>
                        <span> {{__('Settings')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarSettings">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('appSetting')}}">{{__('App Settings')}}</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#sidebarUsers" data-toggle="collapse">
                        <i data-feather="mail"></i>
                        <span> {{__('Users')}} </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarUsers">
                        <ul class="nav-second-level">
{{--                            <li>--}}
{{--                                <a href="{{route('userEntry')}}"><i class="fa fa-plus"></i> {{__('User Entry')}}</a>--}}
{{--                            </li>--}}
                            <li>
                                <a href="{{route('userList')}}"><i class="fa fa-list"></i> {{__('User List')}}</a>
                            </li>
                        </ul>
                    </div>
                </li>


            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->
</div>
